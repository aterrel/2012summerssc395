// Utilities and system includes
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>

#include <cuda_runtime_api.h>
#include "cuda_helper.h"

// Stencil declarations
void jacobiGPU(float *u, const float *f, const int n, const int steps);
void jacobiCPU(float *u, const float *f, const int n, const int steps);
void jacobiHybridNoAsyncPinning(float *u, const float *f, const int n, const int steps);
void jacobiHybridNoAsyncNoPinning(float *u, const float *f, const int n, const int steps);
void jacobiHybridAsyncNoPinning(float *u, const float *f, const int n, const int steps);
void jacobiHybridAsyncPinning(float *u, const float *f, const int n, const int steps);

// Stencil Functions to test
#define NUM_STENCIL_FUNCS 6

typedef void (*stencil_func_t)(float*, const float*, const int, const int);
stencil_func_t stencil_funcs[NUM_STENCIL_FUNCS] = {
  jacobiCPU,
  jacobiGPU,
  jacobiHybridNoAsyncPinning,
  jacobiHybridNoAsyncNoPinning,
  jacobiHybridAsyncNoPinning,
  jacobiHybridAsyncPinning
};

char stencil_func_desc[NUM_STENCIL_FUNCS][128] = {
  "Jacobi CPU Only",
  "Jacobi GPU Only",
  "Jacobi Hybrid No Async Pinning",
  "jacobi Hybrid No Async No Pinning",
  "Jacobi Hybrid Async No Pinning",
  "Jacobi Hybrid Async Pinning"
};


void setF(float *f, int n){
  int j;
  for (j = 0; j < (n+2)*(n+2); ++j) {
    f[j] = 0.0;
  }

  // Set center point to reflect F(center) = 1 elsewhere 0
  float h_sq = 1.0 / (float) n*n;
  if (n % 2 == 1) {
    int c = n/2 + 1;
    f[c + c*(n+2)] = h_sq;
  } else {
    // Here approxmate the center point at four sides
    int c = n/2 + 1;
    f[c + c*(n+2)] = .5*h_sq;
    f[(c+1) + c*(n+2)] = .5*h_sq;
    f[c + (c+1)*(n+2)] = .5*h_sq;
    f[(c+1) + (c+1)*(n+2)] = .5*h_sq;
  }
}


/** Returns the seconds of required for the stencil function.
 */
double timeTest(stencil_func_t stencil_func, float *u, const float *f, const int n, const int steps){
  struct timeval tim;
  double start, end;
  gettimeofday(&tim, NULL);
  start=tim.tv_sec+(tim.tv_usec/1000000.0);

  stencil_func(u, f, n, steps);

  gettimeofday(&tim, NULL);
  end=tim.tv_sec+(tim.tv_usec/1000000.0);
  return end - start;
}

void printDiff(float *data1, float *data2, int width, int height, int len_errors, float error_tol)
{
  printf("Listing first %d Differences > %.6f...\n", len_errors, error_tol);
  int i,j,k;
  int error_count=0;
  for (j = 0; j < height; j++) {
    int row_error_count = 0;
    for (i = 0; i < width; i++) {
      k = j * width + i;
      float diff = fabs(data1[k] - data2[k]);
      if (diff > error_tol) {
	if (row_error_count == 0 && error_count < len_errors){
	  printf(" Row %d:\n", j);
	}
	if (error_count < len_errors) {
	  printf("  Loc(%d,%d)\tdata1=%.5f\tdata2=%.5f\tDiff=%.6f\n", i, j, data1[k], data2[k], diff);
	}
	error_count++;
        row_error_count++;
      }
    }
  }
  printf("Total Errors = %d\n\n", error_count);
}

void testStencils(const float* f, const int n, const int steps)
{
  int i, j;
  // Initialize grid and f
  float *solutions[NUM_STENCIL_FUNCS];
  int num_grid_pts = (n+2)*(n+2);
  for (i = 0; i < NUM_STENCIL_FUNCS; ++i) {
    solutions[i] = (float*) malloc(sizeof(float)*num_grid_pts);
    for (j = 0; j < num_grid_pts; ++j) {
      solutions[i][j] = 0.0;
    }
  }

  // Compare Stencil functions
  printf("Testing stencil functions.\n");
  for(i = 1; i < NUM_STENCIL_FUNCS; ++i) {
    printf(" Diff between %s and %s\n", stencil_func_desc[0], stencil_func_desc[i]);
    printDiff( solutions[0], solutions[i], n, n, steps, 1e-5);
  }

  for (i = 0; i < NUM_STENCIL_FUNCS; ++i){
    free(solutions[i]);
  }
}

void timeStencils(const float *f, const int n, const int steps, const int iters)
{
  int i, j, k;
  int num_grid_pts = (n+2)*(n+2);
  float *solution =  (float*) malloc(sizeof(float)*num_grid_pts);

  // Compare Stencil functions
  printf("\nUsing n=%d and %d steps. Timing based on %d samples \n", n, steps, iters);
  double times[iters];
  for (i = 0; i < NUM_STENCIL_FUNCS; ++i) {
    for (j = 0; j < iters; ++j){
      // Zero out solution
      for (k = 0; k < num_grid_pts; ++k) {
        solution[k] = 0.0;
      }

      times[j] = timeTest(stencil_funcs[i], solution, f, n, steps);
    }
    double avg = 0.0;
    double best = 1e10;
    for (j = 0; j < iters; ++j) {
      avg += times[j];
      best = (best > times[j]) ? times[j] : best;
    }
    avg /= iters;
    printf(">> %s: %.3e s (best), %.3e s (avg)\n", stencil_func_desc[i], best, avg);
  }

  free(solution);
}

////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv)
{
  int i, j;
  printf("%s\n\tStarting stencil GPU/CPU hybrids...\n\n", argv[0]);

  int devID = 0;
  struct cudaDeviceProp props;

  // get number of SMs on this GPU
  HANDLE_ERROR( cudaGetDevice(&devID) );
  HANDLE_ERROR( cudaGetDeviceProperties(&props, devID) );
  HANDLE_ERROR( cudaSetDeviceFlags(cudaDeviceMapHost) );
  printf("Device %d: \"%s\" with Compute %d.%d capability\n", devID, props.name, props.major, props.minor);

  // Optional Command-line size of grid
  int n = 512;
  int steps = n/2;
  int iters = 10;

  int num_grid_pts = (n+2)*(n+2);
  float *f = (float*) malloc(sizeof(float)*num_grid_pts);
  setF(f, n);
  testStencils(f, n, 10);
  timeStencils(f, n, steps, iters);
  free(f);

}
