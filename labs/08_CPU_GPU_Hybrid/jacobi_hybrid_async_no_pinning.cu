#include <cuda.h>
#include <cuda_runtime_api.h>
#include "cuda_helper.h"
#include <stdlib.h>
#include <string.h>

#define BLOCK_SIZE 16
#define CPU_ROW_DIVISOR 16

// GPU/CPU step declarations
__global__ void jacobiGPUStep(const float *u_init, float *u_final, const float *f, const int n, const int m);
extern "C" void jacobiCPUStep(const float *u_init, float *u_final, const float *f, const int n, const int m);


extern "C"
void jacobiHybridAsyncNoPinning(float *u, const float *f, const int n, const int steps)
{
  float *h_u0, *h_u1;
  float *d_u0, *d_u1, *d_f;
  int step;

  int cpu_rows = n / CPU_ROW_DIVISOR;  // Number of row on the CPU
  int gpu_rows = n - cpu_rows;

  size_t cpu_memsize = sizeof(float)*(n+2)*(cpu_rows+2);
  size_t gpu_memsize = sizeof(float)*(n+2)*(gpu_rows+2);
  size_t ghost_memsize = sizeof(float)*(n+2);

  h_u0 = (float *) malloc(cpu_memsize);
  h_u1 = (float *) malloc(cpu_memsize);
  memcpy((void*)h_u0, (void*)u, cpu_memsize);

  HANDLE_ERROR( cudaMalloc((void**) &d_u0, gpu_memsize) );
  HANDLE_ERROR( cudaMalloc((void**) &d_u1, gpu_memsize) );
  HANDLE_ERROR( cudaMalloc((void**) &d_f, gpu_memsize) );

  int grid_cols = n/BLOCK_SIZE;
  int grid_rows = gpu_rows/BLOCK_SIZE;
  dim3 threads(BLOCK_SIZE, BLOCK_SIZE);
  dim3 grid(grid_cols, grid_rows);

  int gpu_offset = (n+2)*cpu_rows;
  HANDLE_ERROR(cudaMemcpyAsync(d_u0, u + gpu_offset, gpu_memsize, cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpyAsync(d_f, f + gpu_offset, gpu_memsize, cudaMemcpyHostToDevice));

  // Take two steps at a time using the same memory
  for(step = 0; step < steps; step += 2){
    if (step != 0){
      // Copy in ghost buffers then into respective arrays
      HANDLE_ERROR( cudaMemcpyAsync(h_u0 + (cpu_rows+1)*(n+2), d_u0 + (n+2), ghost_memsize, cudaMemcpyDeviceToHost) );
      HANDLE_ERROR( cudaMemcpyAsync(d_u0, h_u0 + cpu_rows*(n+2), ghost_memsize, cudaMemcpyHostToDevice) );
    }

    // Run jacobi step on both gpu and cpu
    jacobiGPUStep<<< grid, threads >>>(d_u0, d_u1, d_f, n, gpu_rows);
    jacobiCPUStep(h_u0, h_u1, f, n, cpu_rows);

      // Copy in ghost buffers then into respective arrays
      HANDLE_ERROR( cudaMemcpyAsync(h_u1 + (cpu_rows+1)*(n+2), d_u1 + (n+2), ghost_memsize, cudaMemcpyDeviceToHost) );
      HANDLE_ERROR( cudaMemcpyAsync(d_u1, h_u1 + cpu_rows*(n+2), ghost_memsize, cudaMemcpyHostToDevice) );

    // Run jacobi step on both gpu and cpu
    jacobiGPUStep<<< grid, threads >>>(d_u1, d_u1, d_f, n, gpu_rows);
    jacobiCPUStep(h_u1, h_u1, f, n, cpu_rows);
  }

  // Copy solution to return variable (but not the ghost values)
  HANDLE_ERROR(cudaMemcpyAsync(u+gpu_offset+(n+2), d_u0+n+2, gpu_memsize - sizeof(float)*(n+2), cudaMemcpyDeviceToHost));
  memcpy((void*)u, (void*)h_u0, cpu_memsize);

  // Destroy memory
  HANDLE_ERROR( cudaFree(d_u0) );
  HANDLE_ERROR( cudaFree(d_u1) );
  HANDLE_ERROR( cudaFree(d_f) );
  free(h_u0);
  free(h_u1);
}
