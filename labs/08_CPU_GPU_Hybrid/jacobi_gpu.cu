#include <cuda.h>
#include <cuda_runtime_api.h>
#include "cuda_helper.h"

#define BLOCK_SIZE 16

__global__ void jacobiGPUStep(const float *u_init, float *u_final, const float *f, const int n, const int m)
{
  // Calculate the row index of the Pd element and M
  int i = blockIdx.y*BLOCK_SIZE + threadIdx.y + 1;
  // Calculate the column idenx of Pd and N
  int j = blockIdx.x*BLOCK_SIZE + threadIdx.x + 1;
  float h_sq = (float) (1.0 / n*n);
  if (i > n+1 || j > m+1)
    return;

  u_final[i + j*(n+2)] = .25 * (  u_init[(i-1) + j*(n+2)]
                     		+ u_init[(i+1) + j*(n+2)]
				+ u_init[i     + (j-1)*(n+2)]
				+ u_init[i     + (j+1)*(n+2)]
                                + h_sq*f[i     + j*(n+2)]);
}

extern "C"
void jacobiGPU(float *u, const float *f, const int n, const int steps){
  float *d_u0, *d_u1, *d_f;
  int grid_size = n/BLOCK_SIZE;
  dim3 threads(BLOCK_SIZE, BLOCK_SIZE);
  dim3 grid(grid_size, grid_size);
  //  printf("grid_size %d, block_size %d, total_threads - total_grid %d\n", grid_size, BLOCK_SIZE,
  //          BLOCK_SIZE*BLOCK_SIZE*grid_size*grid_size - n*n);

  size_t mem_size = sizeof(float)*(n+2)*(n+2);
  HANDLE_ERROR(cudaMalloc((void**) &d_u0, mem_size));
  HANDLE_ERROR(cudaMalloc((void**) &d_u1, mem_size));
  HANDLE_ERROR(cudaMalloc((void**) &d_f, mem_size));

  HANDLE_ERROR(cudaMemcpy(d_u0, u, mem_size, cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpy(d_f, f, mem_size, cudaMemcpyHostToDevice));

  int step;
  // Take two steps at a time using the same memory
  for(step = 0; step < steps; step += 2){
    jacobiGPUStep<<< grid, threads >>>(d_u0, d_u1, d_f, n, n);
    jacobiGPUStep<<< grid, threads >>>(d_u1, d_u0, d_f, n, n);
    cudaDeviceSynchronize();
  }
  HANDLE_ERROR(cudaMemcpy(u, d_u0, mem_size, cudaMemcpyDeviceToHost));
  HANDLE_ERROR(cudaFree(d_u0));
  HANDLE_ERROR(cudaFree(d_u1));
  HANDLE_ERROR(cudaFree(d_f));
}
