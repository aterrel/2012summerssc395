#include <stdlib.h>
#include <string.h>

void jacobiCPUStep(const float *u_init, float *u_final, const float *f, const int n, const int m)
{
  int i, j;
  float h_sq = 1.0 / (float) n*n;

  for(j=1; j<m; ++j){
    for(i=1; i<n; ++i){
      u_final[i + j*(n+2)] = .25 * (  u_init[(i-1) + j*(n+2)]
				    + u_init[(i+1) + j*(n+2)]
				    + u_init[i     + (j-1)*(n+2)]
				    + u_init[i     + (j+1)*(n+2)]
                                    + h_sq*f[i     + j*(n+2)]);
    }
  }
}

void jacobiCPU(float *u, const float *f, const int n, const int steps){
  int step;
  size_t memsize = sizeof(float)*(n+2)*(n+2);
  float *u_0 = (float *) malloc(memsize);
  float *u_1 = (float *) malloc(memsize);
  memcpy((void*)u_0, (void*)u, memsize);

  // Take two steps at a time using the same memory
  for(step = 0; step < steps; step += 2){
    jacobiCPUStep(u_0, u_1, f, n, n);
    jacobiCPUStep(u_1, u_0, f, n, n);
  }

  memcpy((void*)u, (void*)u_0, memsize);
  free(u_0);
  free(u_1);
}
