#include <stdio.h>
#include <cuda.h>

#define N   5

__global__ void trans( int *a, int *b ) {
    int tid = blockIdx.x; 
    int x = tid / N;
    int y = tid % N;
    if (tid < (N*N))
        b[y*N + x] = a[x*N + y];
}

int main( void ) {
    int a[N*N], b[N*N];
    int *dev_a, *dev_b;

    // allocate the memory on the GPU
    cudaMalloc( (void**)&dev_a, N * N * sizeof(int) );
    cudaMalloc( (void**)&dev_b, N * N * sizeof(int) );

    // fill the array 'a' 
    for (int i=0; i<N*N; i++) {
        a[i] = i;
    }

    // copy the arrays 'a' and 'b' to the GPU
    cudaMemcpy( dev_a, a, N * N * sizeof(int),
                cudaMemcpyHostToDevice );

    trans<<<N*N,1>>>( dev_a, dev_b);

    // copy the array 'c' back from the GPU to the CPU
    cudaMemcpy( b, dev_b, N * N * sizeof(int),
		cudaMemcpyDeviceToHost );

    // display the results
    printf("Array A\n");
    for (int i=0; i<N; i++) {
        for (int j=0; j<N; j++) {
            printf("%d ", a[i*N + j]);
        }
        printf("\n");
    }

    printf("\nArray B\n");
    for (int i=0; i<N; i++) {
        for (int j=0; j<N; j++) {
            printf("%d ", b[i*N + j]);
        }
        printf("\n");
    }

    // free the memory allocated on the GPU
    cudaFree( dev_a );
    cudaFree( dev_b );

    return 0;
}
