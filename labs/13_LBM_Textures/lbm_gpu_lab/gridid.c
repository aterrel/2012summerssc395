//==============================================================================
// File     : gridId.c
// Revision : 1.0
// Author   : Carlos Rosales [ carlos.rosales.fernandez (at) gmail.com ]
//==============================================================================
// Gives linearized index for a triad of integer values. 
//==============================================================================

__host__ int gridId_h( int i, int j, int k )
{
    return ( i + NX_h*( j + NY_h*k ) );
}

__device__ int gridId( int i, int j, int k )
{
    return ( i + NX*( j + NY*k ) );
}

// NOTE: for GPU this should be ( i + NX*( j + NY*k ) )

