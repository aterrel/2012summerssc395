//==============================================================================
// File     : init.c
// Revision : 1.0
// Author   : Carlos Rosales [ carlos.rosales.fernandez (at) gmail.com ]
//==============================================================================
// Initialize all variables and arrays. 
//==============================================================================

int init( int *nb_east, int *nb_west, int *nb_north, int *nb_south, 
          int *nb_top,  int *nb_bot, float *phi, float *rho, float *ux,
          float *uy, float *uz )
{
    int   i, idx, j, k, m;
    float dx, dy, dz, R, rc;

    // Initialize some constants
    step = 0;

    // Set near neighbors and order parameter
    for( i = 0; i < NX_h; i++){
        for( j = 0; j < NY_h; j++){
            for( k = 0; k < NZ_h; k++){

                idx      = gridId_h( i, j, k );
                phi[idx] = -phiStar;
                rho[idx] = 0.5f*( rhoH + rhoL );
                ux[idx]  = 0.f;
                uy[idx]  = 0.f;
                uz[idx]  = 0.f;

                for( m = 0; m < nBubbles; m++ ){
                    dx = (float)i - bubbles[m][0];
                    dy = (float)j - bubbles[m][1];
                    dz = (float)k - bubbles[m][2];
                    rc = bubbles[m][3];

                    R = sqrtf( dx*dx + dy*dy + dz*dz );
                    if( R <= ( rc + width ) )
                        phi[idx] = phiStar*tanhf( 2.f*( rc - R )/width );
                }

                // Assign neighbor values to arrays
                nb_east[idx]  = i + 1;
                nb_west[idx]  = i - 1;
                nb_north[idx] = j + 1;
                nb_south[idx] = j - 1;
                nb_top[idx]   = k + 1;
                nb_bot[idx]   = k - 1;
            }
        }
    }
    // Fix neighbours at boundaries
    for( j = 0; j < NY_h; j++ ){
        for( k = 0; k < NZ_h; k++ ){
            nb_west[ gridId_h(0     ,j,k) ] = NX_h-1;
            nb_east[ gridId_h(NX_h-1,j,k) ] = 0;
        }
    }
    for( i = 0; i < NX_h; i++ ){
        for( j = 0; j < NY_h; j++ ){
            nb_bot[ gridId_h(i,j,     0) ] = NZ_h-1;
            nb_top[ gridId_h(i,j,NZ_h-1) ] = 0; 
        }
    }
    for( i = 0; i < NX_h; i++ ){
        for( k = 0; k < NZ_h; k++ ){
            nb_south[ gridId_h(i,   0,k) ] = NY_h-1;
            nb_north[ gridId_h(i,NY_h-1,k) ] = 0;
        }
    }


    return 0;
}
