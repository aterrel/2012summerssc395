//==============================================================================
// File     : logsave.c
// Revision : 1.0
// Author   : Carlos Rosales [ carlos.rosales.fernandez (at) gmail.com ]
//==============================================================================
// Save relevant data at the end of the simulation run to file 'runlog.out'.
//==============================================================================

int logSave( char *devName, float devMem,  int *nb_east, int *nb_west, 
             int *nb_north, int *nb_south, int *nb_top,  int *nb_bot,
             float *phi, float *rho, float *ux, float *uy, float *uz )
{
    FILE  *fp, *pipe;
    int   i, idx, ie, iw, j, jn, js, k, kt, kb;
    float Pin, Pout, Ro, Pdif, Perr, Ref, Vef;
    float distMem, hydroMem, nbMem, cpuMem, gpuMem;
    float Umax, Usq, phin, phin2, pressure;
    float gradPhiX, gradPhiY, gradPhiZ, gradPhiSq, lapPhi;
    float p00, p01, p02, p03, p04, p05, p06, p07, p08, p09, p10, p11, p12, p13, 
          p14, p15, p16, p17, p18;
    float pressureIn, pressureOut, volumeIn, volumeOut, lbstep, mlups;
    char  dateStamp[11], timeStamp[6];

    // Initialization
    Ro = bubbles[0][3];
    pressureIn  = 0.f;
    pressureOut = 0.f; 
    volumeIn    = 0.f;
    volumeOut   = 0.f;
    Umax        = 0.f;

    for( k = 0; k < NZ_h; k++ ){
        for( j = 0; j < NY_h; j++ ){
            for( i = 0; i < NX_h; i++ ){

                idx = gridId_h( i, j, k );

                phin  = phi[idx];
                phin2 = phin*phin;                

                // Identify neighbours
                ie = nb_east[idx];
                iw = nb_west[idx];
                jn = nb_north[idx];
                js = nb_south[idx];
                kt = nb_top[idx];
                kb = nb_bot[idx];

                // Nodal values of the order parameter
                p00 = phi[ gridId_h(i ,j ,k ) ];
                p01 = phi[ gridId_h(ie,j ,k ) ];
                p02 = phi[ gridId_h(iw,j ,k ) ];
                p03 = phi[ gridId_h(i ,jn,k ) ];
                p04 = phi[ gridId_h(i ,js,k ) ];
                p05 = phi[ gridId_h(i ,j ,kt) ];
                p06 = phi[ gridId_h(i ,j ,kb) ];
                p07 = phi[ gridId_h(ie,jn,k ) ];
                p08 = phi[ gridId_h(iw,js,k ) ];
                p09 = phi[ gridId_h(ie,js,k ) ];
                p10 = phi[ gridId_h(iw,jn,k ) ];
                p11 = phi[ gridId_h(ie,j ,kt) ];
                p12 = phi[ gridId_h(iw,j ,kb) ];
                p13 = phi[ gridId_h(ie,j ,kb) ];
                p14 = phi[ gridId_h(iw,j ,kt) ];
                p15 = phi[ gridId_h(i ,jn,kt) ];
                p16 = phi[ gridId_h(i ,js,kb) ]; 
                p17 = phi[ gridId_h(i ,jn,kb) ];
                p18 = phi[ gridId_h(i ,js,kt) ];

                // Laplacian of the order parameter
                lapPhi = ( p07 + p08 + p09 + p10 + p11 + p12 + p13 + p14
                       +   p15 + p16 + p17 + p18 + 2.f*( p01 + p02 + p03
                       +   p04 + p05 + p06 - 12.f*p00 ) )*inv6;

                // Components of the order parameter gradient
                gradPhiX = ( 2.f*( p01 - p02 ) + p07 - p08 + p09 - p10
                         +         p11 - p12   + p13 - p14 )*inv12;

                gradPhiY = ( 2.f*( p03 - p04 ) + p07 - p08 + p10 - p09
                         +         p15 - p16   + p17 - p18 )*inv12;

                gradPhiZ = ( 2.f*( p05 - p06 ) + p11 - p12 + p14 - p13
                         +         p15 - p16   + p18 - p17 )*inv12;

                gradPhiSq = gradPhiX*gradPhiX 
                          + gradPhiY*gradPhiY
                          + gradPhiZ*gradPhiZ;

                // Calculate the pressure
                pressure = alpha*( phin2*( 3.f*phin2 - 2.f*phiStarSq ) 
                         - phiStarSq*phiStarSq ) - kappa*( phin*lapPhi 
                         - 0.5f*gradPhiSq ) + Cs_sq*rho[idx];

                if( phi[idx] >= 0.f ){
                    volumeIn = volumeIn + 1.f;
                    pressureIn = pressureIn + pressure;
                }else{
                    volumeOut = volumeOut + 1.f;
                    pressureOut = pressureOut + pressure;
                }

                Usq = ux[idx]*ux[idx] + uy[idx]*uy[idx] + uz[idx]*uz[idx];
                if( Usq > Umax ) Umax = Usq;

            }
        }
    }
    Umax = sqrtf( Umax );

    // Calculate compliance with Laplace's Law ( single bubble, static case )
    Pin  = pressureIn / volumeIn;
    Pout = pressureOut / volumeOut;
    Pdif = Pin - Pout;
    Perr = 0.5f*( 2.f*sigma/Ro - Pdif )*Ro/sigma;

    // Calculate phase conservation
    Ref = pow( volumeIn*invPi*0.75f, 1.f/3.f );
    if( RELAX == 0 ) Vef = volumeIn*invInitVol;
    if( RELAX == 1 ) Vef = volumeIn*invRelaxVol;

    // Estimate memory usage (Mb)
    nbMem    = 6.f*sizeof(int)*gridSize;             // nb_east, nb_west, ...
    hydroMem = 5.f*sizeof(float)*gridSize;           // phi, rho, ux, uy, uz
    distMem  = 2.f*( 19.f*sizeof(float)*gridSize     // g distribution function 
             +        7.f*sizeof(float)*gridSize );  // f distribution function
    gpuMem   = nbMem + hydroMem + distMem;
    cpuMem   = nbMem + hydroMem;

    // Write data to file
    if( RELAX == -1 ){
        fp = fopen( "runlog.out", "w" );

        fprintf( fp, "***\n" );
        fprintf( fp, "*** Multiphase Zheng-Shu-Chew LBM 3D Simulation  \n" );
        fprintf( fp, "*** CUDA Serial Implementation Version 8.0-devel \n" );
        fprintf( fp, "***\n" );

        fprintf( fp, "*** Program running on : %s\n", devName );
        if( devMem >= GB ){
            fprintf( fp, "*** Total GPU Memory   : %.4f GB\n", devMem / GB);
        }else{
            fprintf( fp, "*** Total GPU Memory   : %.4f MB\n", devMem / MB);
        }
        fprintf( fp, "***\n" );

        pipe = popen( "date +%R", "r" );
        fgets( timeStamp, 6, pipe ); pclose( pipe );
        pipe = popen( "date +%F", "r" );
        fgets( dateStamp, 11, pipe ); pclose( pipe );
        fprintf( fp, "*** Simulation started at %s on %s\n", timeStamp, dateStamp );
        fprintf( fp, "***\n\n" );

        fprintf( fp, "INPUT PARAMETERS\n" );
        fprintf( fp, "INPUT PARAMETERS\n" );
        fprintf( fp, "Evolution Steps    = %d\n", maxStep   );
        fprintf( fp, "Relaxation Steps   = %d\n", relaxStep );
        fprintf( fp, "Domain Size in X   = %d\n", NX_h );
        fprintf( fp, "Domain Size in Y   = %d\n", NY_h );
        fprintf( fp, "Domain Size in Z   = %d\n", NZ_h );
        fprintf( fp, "Interface Width    = %6.3f\n", width );
        fprintf( fp, "Interface Tension  = %6.3f\n", sigma );
        fprintf( fp, "Interface Mobility = %6.3f\n", Gamma );
        fprintf( fp, "Low Fluid Density  = %6.3f\n", rhoL );
        fprintf( fp, "High Fluid Density = %6.3f\n", rhoH );
        fprintf( fp, "Tau (Density)      = %6.3f\n", tauRho );
        fprintf( fp, "Tau (Phase)        = %6.3f\n", tauPhi );
        fprintf( fp, "\n" );

        fprintf( fp, "CUDA CONFIGURATION\n" );
        fprintf( fp, "BLOCK_SIZE_X = %d\n", BLOCK_SIZE_X );
        fprintf( fp, "BLOCK_SIZE_Y = %d\n", BLOCK_SIZE_Y );
        fprintf( fp, "\n" );

        if( gpuMem < GB ){
            fprintf( fp, "MEMORY USAGE (Mb)\n" );
            fprintf( fp, "Distribution Arrays   = %4.2f\n", distMem / MB );
            fprintf( fp, "Auxiliary Arrays      = %4.2f\n", hydroMem / MB );
            fprintf( fp, "Total CPU Memory Used = %4.2f\n", cpuMem / MB );
            fprintf( fp, "Total GPU Memory Used = %4.2f\n", gpuMem / MB );
        }else{
            fprintf( fp, "MEMORY USAGE (Mb)\n" );
            fprintf( fp, "Distribution Arrays   = %4.2f\n", distMem / GB );
            fprintf( fp, "Auxiliary Arrays      = %4.2f\n", hydroMem / GB );
            fprintf( fp, "Total CPU Memory Used = %4.2f\n", cpuMem / GB );
            fprintf( fp, "Total GPU Memory Used = %4.2f\n", gpuMem / GB );
        }
        fprintf( fp, "\n" );
        fclose( fp );
    }
    else if( RELAX == 1 ){
        fp = fopen( "runlog.out", "a" );

        fprintf( fp, "RELAXATION RESULTS\n" );
        fprintf( fp, "Effective Radius   = %6.3f\n", Ref );
        fprintf( fp, "Phase Conservation = %6.3f\n", Vef );
        fprintf( fp, "(Pin - Pout)       = %6.3f\n", Pdif );
        fprintf( fp, "Laplace Error      = %6.3f\n", Perr );
        fprintf( fp, "Parasitic Velocity = %6.3f\n", Umax );
        fprintf( fp, "\n" );

        fclose( fp );
    }else{
        fp = fopen( "runlog.out", "a" );

        fprintf( fp, "OUTPUT RESULTS\n" );
        fprintf( fp, "Effective Radius   = %6.3f\n", Ref );
        fprintf( fp, "Phase Conservation = %6.3f\n", Vef );
        fprintf( fp, "Maximum Velocity   = %6.3f\n", Umax );
        fprintf( fp, "\n" );

        lbstep = mainTime * 0.001f / ( (float)maxStep );
        mlups  = (float)maxStep * (float)gridSize * 0.000001f / ( mainTime * 0.001f );
        fprintf( fp, "TIMING INFORMATION (seconds)\n" );
        fprintf( fp, "Setup Time      = %6.3f\n", setupTime*0.001f );
        fprintf( fp, "Relaxation Time = %6.3f\n", relaxTime*0.001f );
        fprintf( fp, "Main Loop Time  = %6.3f\n", mainTime*0.001f );
        fprintf( fp, "LBM step Time   = %6.3f\n", lbstep );
        fprintf( fp, "MLUPS           = %6.3f\n", mlups );
        fprintf( fp, "\n" );

        pipe = popen( "date +%R", "r" );
        fgets( timeStamp, 6, pipe ); pclose( pipe );
        pipe = popen( "date +%F", "r" );
        fgets( dateStamp, 11, pipe ); pclose( pipe );
        fprintf( fp, "***\n" );
        fprintf( fp, "*** Simulation completed at %s on %s\n", timeStamp, dateStamp );
        fprintf( fp, "***\n" );
        
        fclose( fp );
    }

    return 0;
}
