//==============================================================================
// File     : update_velocity.cu
// Revision : 1.0
// Author   : Carlos Rosales [ carlos.rosales.fernandez (at) gmail.com ]
//==============================================================================
// Update fluid velocity.
//==============================================================================

__global__ void update_velocity( float *rho_d,  float *ux_d,   float *uy_d,
                                 float *uz_d,   float *g_1_d,  float *g_2_d,
                                 float *g_3_d,  float *g_4_d,  float *g_5_d,
                                 float *g_6_d,  float *g_7_d,  float *g_8_d,
                                 float *g_9_d,  float *g_10_d, float *g_11_d,
                                 float *g_12_d, float *g_13_d, float *g_14_d,
                                 float *g_15_d, float *g_16_d, float *g_17_d,
                                 float *g_18_d )
{
//==============================================================================
// 6 Registers | 23 arguments 
//==============================================================================
    int   i, idx, idxTmp, j, k;
    float invRho;
//==============================================================================

    // Identify current thread
    i = blockIdx.x * blockDim.x + threadIdx.x;
    j = blockIdx.y * blockDim.y + threadIdx.y;
    idxTmp = i + NX * j;

    // Order parameter update
    // PERFORMANCE NOTE: try using separate loops and test performance
    for( k = 0; k < NZ; k++ ){

        idx    = idxTmp + NXY * k;
        invRho = 1.0f / rho_d[idx];

        ux_d[idx] = ( g_1_d[idx]  - g_2_d[idx]  + g_7_d[idx]  - g_8_d[idx]
                  +   g_9_d[idx]  - g_10_d[idx] + g_11_d[idx] - g_12_d[idx]
                  +   g_13_d[idx] - g_14_d[idx] ) * invRho;

        uy_d[idx] = ( g_3_d[idx]  - g_4_d[idx]  + g_7_d[idx]  - g_8_d[idx]
                  -   g_9_d[idx]  + g_10_d[idx] + g_15_d[idx] - g_16_d[idx]
                  +   g_17_d[idx] - g_18_d[idx] ) * invRho;

        uz_d[idx] = ( g_5_d[idx]  - g_6_d[idx]  + g_11_d[idx] - g_12_d[idx]
                  -   g_13_d[idx] + g_14_d[idx] + g_15_d[idx] - g_16_d[idx]
                  -   g_17_d[idx] + g_18_d[idx] ) * invRho;

    }

}

