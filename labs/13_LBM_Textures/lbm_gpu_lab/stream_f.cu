//==============================================================================
// File     : stream_f.c
// Revision : 1.0
// Author   : Carlos Rosales [ carlos.rosales.fernandez (at) gmail.com ]
//==============================================================================
// Streaming step for the order parameter distribution function f. 
//==============================================================================

__global__ void stream_f( int *nb_east_d,  int *nb_west_d, int *nb_north_d,
                          int *nb_south_d, int *nb_top_d,  int *nb_bot_d, 
                          float *f_0_d, float *f_1_d, float *f_2_d, 
                          float *f_3_d, float *f_4_d, float *f_5_d, 
                          float *f_6_d )
{
//==============================================================================
// 11 Registers | 11 arguments
//==============================================================================
    int i, idx, idx2, ie, iw, j, jn, js, k, kt, kb;
//==============================================================================

    // Identify current thread
    i = blockIdx.x * blockDim.x + threadIdx.x;
    j = blockIdx.y * blockDim.y + threadIdx.y;

    for( k = 0; k < NZ; k++ ){

        idx  = gridId( i, j, k );
        idx2 = gridId( i, j, k ) + dcol;

        ie = nb_east_d[idx];
        iw = nb_west_d[idx];
        jn = nb_north_d[idx];
        js = nb_south_d[idx];
        kt = nb_top_d[idx];
        kb = nb_bot_d[idx];

        f_0_d[idx] = f_0_d[idx2];

        f_1_d[ gridId(ie,j,k) ] = eta_d*f_1_d[idx2] + eta2_d*f_1_d[ gridId(ie,j,k) + dcol ];
        f_2_d[ gridId(iw,j,k) ] = eta_d*f_2_d[idx2] + eta2_d*f_2_d[ gridId(iw,j,k) + dcol ];
        f_3_d[ gridId(i,jn,k) ] = eta_d*f_3_d[idx2] + eta2_d*f_3_d[ gridId(i,jn,k) + dcol ];
        f_4_d[ gridId(i,js,k) ] = eta_d*f_4_d[idx2] + eta2_d*f_4_d[ gridId(i,js,k) + dcol ];
        f_5_d[ gridId(i,j,kt) ] = eta_d*f_5_d[idx2] + eta2_d*f_5_d[ gridId(i,j,kt) + dcol ];
        f_6_d[ gridId(i,j,kb) ] = eta_d*f_6_d[idx2] + eta2_d*f_6_d[ gridId(i,j,kb) + dcol ];

    }

}



