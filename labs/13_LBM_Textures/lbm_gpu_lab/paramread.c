//==============================================================================
// File     : paramread.c
// Revision : 1.0
// Author   : Carlos Rosales [ carlos.rosales.fernandez (at) gmail.com ]
//==============================================================================
// Read input parameters from ascii file "properties.in". 
//==============================================================================

int paramRead()
{
    FILE *fp;
    char tmp[32];
    int  i;
    float invTauRho, invTauRhoHalf;

    // Get simulation data from primary configuration file
    fp = fopen( "properties.in", "r" );

    fscanf( fp, "%s %s %s %s", tmp, tmp, tmp, tmp );
    fscanf( fp, "%d %d %d %d", &maxStep, &relaxStep, &save, &stat );

    fscanf( fp, "%s %s %s", tmp, tmp, tmp );
    fscanf( fp, "%d %d %d", &NX_h, &NY_h, &NZ_h );

    fscanf( fp, "%s %s", tmp, tmp );
    fscanf( fp, "%d %d", &BLOCK_SIZE_X, &BLOCK_SIZE_Y );

    fscanf( fp, "%s %s", tmp, tmp );
    fscanf( fp, "%f %f", &rhoL, &rhoH );
     
    fscanf( fp, "%s %s", tmp, tmp );
    fscanf( fp, "%f %f", &tauRho, &tauPhi );

    fscanf( fp, "%s %s %s", tmp, tmp, tmp );
    fscanf( fp, "%f %f %f", &width, &sigma, &Gamma );

    fscanf( fp, "%s", tmp );
    fscanf( fp, "%f", &Eo );

    fclose( fp );

    // Get discrete phase data from secondary configuration file
    fp = fopen( "discrete.in", "r" );
    
    fscanf( fp, "%s", tmp );
    fscanf( fp, "%d", &nBubbles );

    fscanf( fp, "%s %s %s %s", tmp, tmp, tmp, tmp );
    for( i = 0; i < nBubbles; i++){
        fscanf( fp, "%f %f", &bubbles[i][0], &bubbles[i][1] ); // xc(i), yc(i)
        fscanf( fp, "%f %f", &bubbles[i][2], &bubbles[i][3] ); // zc(i), Rc(i)
    }
    fclose( fp );

    // Define the domain size
    gridSize = NX_h*NY_h*NZ_h;
    NXY_h    = NX_h*NY_h;

    // Fluid properties
    phiStar       = 0.5f*( rhoH - rhoL );
    phiStarSq     = phiStar*phiStar;
    invTauRho     = 1.f/tauRho;
    invTauRhoOne  = 1.f - invTauRho;
    invTauRhoHalf = 1.f - 0.5f*invTauRho;

    eta          = 1.f/( tauPhi + 0.5f );
    eta2         = 1.f - eta;
    invEta2      = 0.5f/( 1.f - eta );
    invTauPhi    = 1.f/tauPhi;
    invTauPhiOne = 1.f - invTauPhi;

    // Chemical Potential Stuff
    alpha  = 0.75f*sigma/( width*phiStarSq*phiStarSq );
    alpha4 = alpha*4.f;
    kappa  = ( width*phiStar )*( width*phiStar )*alpha*0.5f;

    // Rc = bubbles(1,4) Use the first bubble as the largest in the system
    gravity = 0.25f*Eo*sigma/( ( rhoH - rhoL )*bubbles[0][3]*bubbles[0][3] );
    grav    = 2.f*phiStar*gravity;

    // Modified LBM parameters
    Wn0  = invTauRho*W0;
    Wn1  = invTauRho*W1;
    Wn2  = invTauRho*W2;

    WnC0 = invTauRhoHalf*WC0;
    WnC1 = invTauRhoHalf*WC1;
    WnC2 = invTauRhoHalf*WC2;


    return 0;
}

