//==============================================================================
// File     : update_phi.cu
// Revision : 1.0
// Author   : Carlos Rosales [ carlos.rosales.fernandez (at) gmail.com ]
//==============================================================================
// Update order parameter.
//==============================================================================

__global__ void update_phi( float *phi_d, float *f_0_d, float *f_1_d,
                            float *f_2_d, float *f_3_d, float *f_4_d,
                            float *f_5_d, float *f_6_d )
{
//==============================================================================
// 4 Registers | 8 arguments
//==============================================================================
    int i, idx, j, k, idxTmp;
//==============================================================================

    // Identify current thread
    i = blockIdx.x * blockDim.x + threadIdx.x;
    j = blockIdx.y * blockDim.y + threadIdx.y;
    idxTmp = i + NX * j;

    // Order parameter update
    for( k = 0; k < NZ; k++ ){
        idx = idxTmp + NXY * k;
        phi_d[idx] = f_0_d[idx] + f_1_d[idx] + f_2_d[idx] + f_3_d[idx]
                   + f_4_d[idx] + f_5_d[idx] + f_6_d[idx];
    }

}

