//==============================================================================
// Program  : ZSC-3D-MPI
// Revision : 1.0
// Author   : Carlos Rosales [ carlos.rosales.fernandez (at) gmail.com ]
//==============================================================================
// The values of RELAX are used to control output:
// RELAX = -1 -> Setup Stage Completed
// RELAX =  0 -> Relaxation Stage Completed
// RELAX =  1 -> Main Calculation Stage Completed
//==============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cuda.h>
#include "constants.h"

// CPU functions
#include "gridid.c"
#include "init.c"
#include "paramread.c"
#include "logsave.c"
#include "stats.c"
#include "vtksave.c"

// GPU kernels
#include "init_f.cu"
#include "init_g.cu"
#include "update_phi.cu"
#include "update_rho.cu"
#include "update_velocity.cu"
#include "collision_f.cu"
#include "collision_g.cu"
#include "stream_f.cu"
#include "stream_g.cu"

int fatalError( char *errorStr )
{
    fprintf( stderr, "ERROR: %s.\n", errorStr );
    return 1;
}

int main( int argc, char **argv )
{
    int   device = 0;
    float cpuMem, devMem, distMem, gpuMem, hydroMem, nbMem;
    char  devName[32];

    // choose the GPU for execution
    cudaSetDevice(device);
    cudaDeviceProp properties;
    cudaGetDeviceProperties(&properties, device);

    // Declare and create timing events
    cudaEvent_t start, stopSetup, stopRelax, stopMain;
    cudaEventCreate( &start );
    cudaEventCreate( &stopSetup  );
    cudaEventCreate( &stopRelax  );
    cudaEventCreate( &stopMain  );


    fprintf( stdout, "*** Multiphase Zheng-Shu-Chew LBM 3D Simulation   ***\n" );
    fprintf( stdout, "*** CUDA Serial Implementation Version 7.0-devel  ***\n" );
    fprintf( stdout, "\n" );

    strncpy( devName, properties.name, 32 );
    devMem = properties.totalGlobalMem;
    fprintf( stdout, "*** Program running on : %s\n", devName );
    if( devMem >= GB ){
        fprintf( stdout, "*** Total GPU Memory   : %.4f GB\n\n", devMem / GB);
    }else{
        fprintf( stdout, "*** Total GPU Memory   : %.4f MB\n\n", devMem / MB);
    }


//====================== ARRAY DECLARATIONS ====================================
    // Host array declarations. These arrays have dinensions NX*NY*NZ, and
    // can be traversed using the running index idx = k + NZ*( j + NY*i )
    int   *nb_east = 0, *nb_west = 0, *nb_north = 0, *nb_south = 0, 
          *nb_top  = 0, *nb_bot  = 0;

    float *phi = 0, *rho = 0, *ux = 0, *uy = 0, *uz = 0;

    // Device array declarations. These arrays have dinensions NX*NY*NZ, and
    // can be traversed using the running index idx = k + NZ*( j + NY*i )
    int   *nb_east_d = 0, *nb_west_d = 0, *nb_north_d = 0, *nb_south_d = 0, 
          *nb_top_d  = 0, *nb_bot_d  = 0;

    float *phi_d = 0, *rho_d = 0, *ux_d = 0, *uy_d = 0, *uz_d = 0;

    float *f_0_d = 0, *f_1_d = 0, *f_2_d = 0, *f_3_d = 0, *f_4_d = 0,
          *f_5_d = 0, *f_6_d = 0;

    float *g_0_d  = 0, *g_1_d  = 0, *g_2_d  = 0, *g_3_d  = 0, *g_4_d  = 0,
          *g_5_d  = 0, *g_6_d  = 0, *g_7_d  = 0, *g_8_d  = 0, *g_9_d  = 0,
          *g_10_d = 0, *g_11_d = 0, *g_12_d = 0, *g_13_d = 0, *g_14_d = 0,
          *g_15_d = 0, *g_16_d = 0, *g_17_d = 0, *g_18_d = 0;


//====================== READ INPUT PARAMETERS =================================
    cudaEventRecord( start, 0 );
    RELAX = -1;
    paramRead();

    // Sanity check for partition configuration
    if( BLOCK_SIZE_X*BLOCK_SIZE_Y > 512 ) fatalError( "Too many blocks" );
    if( NX_h < BLOCK_SIZE_X ) fatalError( "BLOCK_SIZE_X is too large");
    if( NY_h < BLOCK_SIZE_Y ) fatalError( "BLOCK_SIZE_Y is too large");

    // Define the grid and the number of threads per block
    dim3 dimblock( BLOCK_SIZE_X, BLOCK_SIZE_Y, 1 );
    dim3 dimgrid( NX_h/BLOCK_SIZE_X, NY_h/BLOCK_SIZE_Y, 1 );

//====================== HOST ARRAY MEMORY ALLOCATION ==========================
    nb_east  = (int *)malloc( gridSize*sizeof(int) );
    nb_west  = (int *)malloc( gridSize*sizeof(int) );
    nb_north = (int *)malloc( gridSize*sizeof(int) );
    nb_south = (int *)malloc( gridSize*sizeof(int) );
    nb_top   = (int *)malloc( gridSize*sizeof(int) );
    nb_bot   = (int *)malloc( gridSize*sizeof(int) );

    phi = (float *)malloc( gridSize*sizeof(float) );
    rho = (float *)malloc( gridSize*sizeof(float) );

    ux = (float *)malloc( gridSize*sizeof(float) );
    uy = (float *)malloc( gridSize*sizeof(float) );
    uz = (float *)malloc( gridSize*sizeof(float) );

//====================== DEVICE ARRAY MEMORY ALLOCATION ========================
    cudaMalloc( (void **) &nb_east_d,  gridSize*sizeof(int) );
    cudaMalloc( (void **) &nb_west_d,  gridSize*sizeof(int) );
    cudaMalloc( (void **) &nb_north_d, gridSize*sizeof(int) );
    cudaMalloc( (void **) &nb_south_d, gridSize*sizeof(int) );
    cudaMalloc( (void **) &nb_top_d,   gridSize*sizeof(int) );
    cudaMalloc( (void **) &nb_bot_d,   gridSize*sizeof(int) );

    cudaMalloc( (void **) &phi_d, gridSize*sizeof(float) );
    cudaMalloc( (void **) &rho_d, gridSize*sizeof(float) );

    cudaMalloc( (void **) &ux_d, gridSize*sizeof(float) );
    cudaMalloc( (void **) &uy_d, gridSize*sizeof(float) );
    cudaMalloc( (void **) &uz_d, gridSize*sizeof(float) );

    cudaMalloc( (void **) &f_0_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &f_1_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &f_2_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &f_3_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &f_4_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &f_5_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &f_6_d, 2*gridSize*sizeof(float) );

    cudaMalloc( (void **) &g_0_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &g_1_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &g_2_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &g_3_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &g_4_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &g_5_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &g_6_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &g_7_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &g_8_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &g_9_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &g_10_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &g_11_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &g_12_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &g_13_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &g_14_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &g_15_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &g_16_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &g_17_d, 2*gridSize*sizeof(float) );
    cudaMalloc( (void **) &g_18_d, 2*gridSize*sizeof(float) );

//====================== DEVICE ARRAY ALLOCATION TEST ==========================
    if( nb_east_d  == 0 || nb_west_d == 0 || nb_north_d == 0 ||  
        nb_south_d == 0 || nb_top_d  == 0 || nb_bot_d   == 0 ){
        fatalError( "Unable to allocate memory for neighbors on device" );
    }
    if( phi_d == 0 || rho_d == 0 || ux_d == 0 || uy_d == 0 || uz_d == 0 ){
        fatalError( "Unable to allocate memory for hydro variables on device" );
    }
    if( f_0_d == 0 || f_1_d == 0 || f_2_d == 0 || f_3_d == 0 || 
        f_4_d == 0 || f_5_d == 0 || f_6_d == 0){
        fatalError( "Unable to allocate memory for f on device" );
    }
    if( g_0_d  == 0 || g_1_d  == 0 || g_2_d  == 0 || g_3_d  == 0 || 
        g_4_d  == 0 || g_5_d  == 0 || g_6_d  == 0 || g_7_d  == 0 ||
        g_8_d  == 0 || g_9_d  == 0 || g_10_d == 0 || g_11_d == 0 ||
        g_12_d == 0 || g_13_d == 0 || g_14_d == 0 || g_15_d == 0 ||
        g_16_d == 0 || g_17_d == 0 || g_18_d == 0 ){
        fatalError( "Unable to allocate memory for g on device" );
    }


    nbMem    = 6.f*sizeof(int)*gridSize;
    hydroMem = 5.f*sizeof(float)*gridSize;
    distMem  = 2.f*( 19.f*sizeof(float)*gridSize + 7.f*sizeof(float)*gridSize );
    gpuMem   = nbMem + hydroMem + distMem;
    cpuMem   = nbMem + hydroMem;
    if( cpuMem >= GB )
        fprintf( stdout, "Allocated %.4f GB or memory on host.\n", cpuMem/GB );
    if( cpuMem < GB )
        fprintf( stdout, "Allocated %.4f MB or memory on host.\n", cpuMem/MB );
    if( gpuMem >= GB )
        fprintf( stdout, "Allocated %.4f GB or memory on device.\n", gpuMem/GB );
    if( gpuMem < GB )
        fprintf( stdout, "Allocated %.4f MB or memory on device.\n", gpuMem/MB );

//====================== INITIALIZE DATA =======================================
    init( nb_east, nb_west, nb_north, nb_south, nb_top, nb_bot, phi, rho, ux, 
          uy, uz );

    logSave( devName, devMem, nb_east, nb_west, nb_north, nb_south, nb_top, 
             nb_bot, phi, rho, ux, uy, uz );
    RELAX = 1; 

    stats( phi, ux, uy, uz );

    vtkSave( nb_east, nb_west, nb_north, nb_south, nb_top, nb_bot, phi, 
             rho, ux, uy, uz );

    cudaMemcpy( nb_east_d,  nb_east,  sizeof(int)*gridSize, cudaMemcpyHostToDevice );
    cudaMemcpy( nb_west_d,  nb_west,  sizeof(int)*gridSize, cudaMemcpyHostToDevice ); 
    cudaMemcpy( nb_north_d, nb_north, sizeof(int)*gridSize, cudaMemcpyHostToDevice ); 
    cudaMemcpy( nb_south_d, nb_south, sizeof(int)*gridSize, cudaMemcpyHostToDevice ); 
    cudaMemcpy( nb_top_d,   nb_top,   sizeof(int)*gridSize, cudaMemcpyHostToDevice ); 
    cudaMemcpy( nb_bot_d,   nb_bot,   sizeof(int)*gridSize, cudaMemcpyHostToDevice ); 

    cudaMemcpy( phi_d, phi, sizeof(float)*gridSize, cudaMemcpyHostToDevice );
    cudaMemcpy( rho_d, rho, sizeof(float)*gridSize, cudaMemcpyHostToDevice );
    cudaMemcpy( ux_d,  ux,  sizeof(float)*gridSize, cudaMemcpyHostToDevice );
    cudaMemcpy( uy_d,  uy,  sizeof(float)*gridSize, cudaMemcpyHostToDevice );
    cudaMemcpy( uz_d,  uz,  sizeof(float)*gridSize, cudaMemcpyHostToDevice );

    cudaMemcpyToSymbol( dcol, &gridSize, sizeof(int), 0, cudaMemcpyHostToDevice );
    cudaMemcpyToSymbol( NX,   &NX_h,  sizeof(int), 0, cudaMemcpyHostToDevice );
    cudaMemcpyToSymbol( NY,   &NY_h,  sizeof(int), 0, cudaMemcpyHostToDevice );
    cudaMemcpyToSymbol( NZ,   &NZ_h,  sizeof(int), 0, cudaMemcpyHostToDevice );
    cudaMemcpyToSymbol( NXY,  &NXY_h, sizeof(int), 0, cudaMemcpyHostToDevice );

    cudaMemcpyToSymbol( K0, &Wn0, sizeof(float), 0, cudaMemcpyHostToDevice );
    cudaMemcpyToSymbol( K1, &Wn1, sizeof(float), 0, cudaMemcpyHostToDevice );
    cudaMemcpyToSymbol( K2, &Wn2, sizeof(float), 0, cudaMemcpyHostToDevice );

    cudaMemcpyToSymbol( KC0, &WnC0, sizeof(float), 0, cudaMemcpyHostToDevice );
    cudaMemcpyToSymbol( KC1, &WnC1, sizeof(float), 0, cudaMemcpyHostToDevice );
    cudaMemcpyToSymbol( KC2, &WnC2, sizeof(float), 0, cudaMemcpyHostToDevice );

    cudaMemcpyToSymbol( alpha4_d,    &alpha4,    sizeof(float), 0, cudaMemcpyHostToDevice );
    cudaMemcpyToSymbol( kappa_d,     &kappa,     sizeof(float), 0, cudaMemcpyHostToDevice );
    cudaMemcpyToSymbol( Gamma_d,     &Gamma,     sizeof(float), 0, cudaMemcpyHostToDevice );
    cudaMemcpyToSymbol( eta_d,       &eta,       sizeof(float), 0, cudaMemcpyHostToDevice );
    cudaMemcpyToSymbol( eta2_d,      &eta2,      sizeof(float), 0, cudaMemcpyHostToDevice );
    cudaMemcpyToSymbol( phiStarSq_d, &phiStarSq, sizeof(float), 0, cudaMemcpyHostToDevice );

    cudaMemcpyToSymbol( invEta2_d,      &invEta2,      sizeof(float), 0, cudaMemcpyHostToDevice );
    cudaMemcpyToSymbol( invTauPhi_d,    &invTauPhi,    sizeof(float), 0, cudaMemcpyHostToDevice );
    cudaMemcpyToSymbol( invTauPhiOne_d, &invTauPhiOne, sizeof(float), 0, cudaMemcpyHostToDevice );
    cudaMemcpyToSymbol( invTauRhoOne_d, &invTauRhoOne, sizeof(float), 0, cudaMemcpyHostToDevice );

    init_f <<<dimgrid,dimblock>>> ( nb_east_d, nb_west_d, nb_north_d, nb_south_d,
                                    nb_top_d, nb_bot_d, phi_d, f_0_d, f_1_d, 
                                    f_2_d, f_3_d, f_4_d, f_5_d, f_6_d );

    init_g <<<dimgrid,dimblock>>> ( nb_east_d, nb_west_d, nb_north_d, nb_south_d,
                                    nb_top_d, nb_bot_d, phi_d, rho_d, g_0_d,
                                    g_1_d, g_2_d, g_3_d, g_4_d, g_5_d, g_6_d,
                                    g_7_d, g_8_d, g_9_d, g_10_d, g_11_d, g_12_d,
                                    g_13_d, g_14_d, g_15_d, g_16_d, g_17_d, g_18_d );

    fprintf( stdout, "\nData initialization completed.\n" );
    cudaThreadSynchronize();
    cudaEventRecord( stopSetup, 0 );

//====================== INTERFACE RELAXATION LOOP =============================
    RELAX_GRAV = 0.f;
    cudaMemcpyToSymbol( grav_d, &RELAX_GRAV, sizeof(float), 0, cudaMemcpyHostToDevice );
    for( step = 1; step <= relaxStep; step++){

        update_phi <<<dimgrid,dimblock>>> ( phi_d, f_0_d, f_1_d, f_2_d, f_3_d,
                                            f_4_d, f_5_d, f_6_d );

        update_rho <<<dimgrid,dimblock>>> ( rho_d, g_0_d, g_1_d, g_2_d, g_3_d, 
                                            g_4_d, g_5_d, g_6_d, g_7_d, g_8_d, 
                                            g_9_d, g_10_d, g_11_d, g_12_d, 
                                            g_13_d, g_14_d, g_15_d, g_16_d, 
                                            g_17_d, g_18_d );

        update_velocity <<<dimgrid,dimblock>>> ( rho_d, ux_d, uy_d, uz_d, g_1_d, 
                                                 g_2_d, g_3_d, g_4_d, g_5_d, 
                                                 g_6_d, g_7_d, g_8_d, g_9_d, 
                                                 g_10_d, g_11_d, g_12_d, g_13_d, 
                                                 g_14_d, g_15_d, g_16_d, g_17_d,
                                                 g_18_d );

        collision_f <<<dimgrid,dimblock>>> ( nb_east_d,  nb_west_d,
                                             nb_north_d, nb_south_d,
                                             nb_top_d,  nb_bot_d, phi_d,
                                             rho_d, ux_d, uy_d, uz_d, 
                                             f_0_d, f_1_d, f_2_d,
                                             f_3_d, f_4_d, f_5_d, f_6_d );

        collision_g <<<dimgrid,dimblock>>> ( nb_east_d,  nb_west_d, 
                                             nb_north_d, nb_south_d, 
                                             nb_top_d,   nb_bot_d,
                                             phi_d, rho_d, ux_d, uy_d, 
                                             uz_d,  g_0_d, g_1_d, g_2_d, 
                                             g_3_d, g_4_d, g_5_d, g_6_d,
                                             g_7_d, g_8_d, g_9_d, g_10_d,
                                             g_11_d, g_12_d, g_13_d, 
                                             g_14_d, g_15_d, g_16_d, 
                                             g_17_d, g_18_d );

        stream_f <<<dimgrid,dimblock>>> ( nb_east_d,  nb_west_d, nb_north_d,
                                          nb_south_d, nb_top_d,  nb_bot_d, 
                                          f_0_d, f_1_d, f_2_d, f_3_d, f_4_d, 
                                          f_5_d, f_6_d );

        stream_g <<<dimgrid,dimblock>>> ( nb_east_d,  nb_west_d, nb_north_d,
                                          nb_south_d, nb_top_d,  nb_bot_d,
                                          g_0_d,  g_1_d,  g_2_d,  g_3_d, g_4_d,
                                          g_5_d,  g_6_d,  g_7_d,  g_8_d, g_9_d,
                                          g_10_d, g_11_d, g_12_d, g_13_d, 
                                          g_14_d, g_15_d, g_16_d, g_17_d,
                                          g_18_d );

        if( (step%stat) == 0 ){
            cudaThreadSynchronize();
            cudaMemcpy( phi, phi_d, sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
            cudaMemcpy( ux,  ux_d,  sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
            cudaMemcpy( uy,  uy_d,  sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
            cudaMemcpy( uz,  uz_d,  sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
            stats( phi, ux, uy, uz );
        }
        if( (step%save) == 0 ){
            cudaThreadSynchronize();
            cudaMemcpy( phi, phi_d, sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
            cudaMemcpy( rho, rho_d, sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
            cudaMemcpy( ux,  ux_d,  sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
            cudaMemcpy( uy,  uy_d,  sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
            cudaMemcpy( uz,  uz_d,  sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
            vtkSave( nb_east, nb_west, nb_north, nb_south, nb_top, nb_bot,
                     phi, rho, ux, uy, uz );
        }
    }
    logSave( devName, devMem, nb_east, nb_west, nb_north, nb_south, nb_top, 
             nb_bot, phi, rho, ux, uy, uz );
    fprintf( stdout, "\nRelaxation run completed.\n" );
    RELAX = 0;
    cudaMemcpyToSymbol( grav_d, &grav, sizeof(float), 0, cudaMemcpyHostToDevice );
     

//====================== SAVE RELAXED CONFIGURATION ============================
    cudaThreadSynchronize();
    cudaMemcpy( phi, phi_d, sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
    cudaMemcpy( rho, rho_d, sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
    cudaMemcpy( ux,  ux_d,  sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
    cudaMemcpy( uy,  uy_d,  sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
    cudaMemcpy( uz,  uz_d,  sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
    step = 0;
    stats( phi, ux, uy, uz );
    vtkSave( nb_east, nb_west, nb_north, nb_south, nb_top, nb_bot, phi, 
             rho, ux, uy, uz );
    cudaEventRecord( stopRelax, 0 );

//====================== MAIN CALCULATION LOOP =================================
    for( step = 1; step < maxStep; step++){

        update_phi <<<dimgrid,dimblock>>> ( phi_d, f_0_d, f_1_d, f_2_d, f_3_d,
                                            f_4_d, f_5_d, f_6_d );

        update_rho <<<dimgrid,dimblock>>> ( rho_d, g_0_d, g_1_d, g_2_d, g_3_d, 
                                            g_4_d, g_5_d, g_6_d, g_7_d, g_8_d, 
                                            g_9_d, g_10_d, g_11_d, g_12_d, 
                                            g_13_d, g_14_d, g_15_d, g_16_d, 
                                            g_17_d, g_18_d );

        update_velocity <<<dimgrid,dimblock>>> ( rho_d, ux_d, uy_d, uz_d, g_1_d, 
                                                 g_2_d, g_3_d, g_4_d, g_5_d, 
                                                 g_6_d, g_7_d, g_8_d, g_9_d, 
                                                 g_10_d, g_11_d, g_12_d, g_13_d, 
                                                 g_14_d, g_15_d, g_16_d, g_17_d,
                                                 g_18_d );

        collision_f <<<dimgrid,dimblock>>> ( nb_east_d,  nb_west_d,
                                             nb_north_d, nb_south_d,
                                             nb_top_d,  nb_bot_d, phi_d,
                                             rho_d, ux_d, uy_d, uz_d, 
                                             f_0_d, f_1_d, f_2_d,
                                             f_3_d, f_4_d, f_5_d, f_6_d );

        collision_g <<<dimgrid,dimblock>>> ( nb_east_d,  nb_west_d, 
                                             nb_north_d, nb_south_d, 
                                             nb_top_d,   nb_bot_d,
                                             phi_d, rho_d, ux_d, uy_d, 
                                             uz_d,  g_0_d, g_1_d, g_2_d, 
                                             g_3_d, g_4_d, g_5_d, g_6_d,
                                             g_7_d, g_8_d, g_9_d, g_10_d,
                                             g_11_d, g_12_d, g_13_d, 
                                             g_14_d, g_15_d, g_16_d, 
                                             g_17_d, g_18_d );

        stream_f <<<dimgrid,dimblock>>> ( nb_east_d,  nb_west_d, nb_north_d,
                                          nb_south_d, nb_top_d,  nb_bot_d, 
                                          f_0_d, f_1_d, f_2_d, f_3_d, f_4_d, 
                                          f_5_d, f_6_d );

        stream_g <<<dimgrid,dimblock>>> ( nb_east_d,  nb_west_d, nb_north_d,
                                          nb_south_d, nb_top_d,  nb_bot_d,
                                          g_0_d,  g_1_d,  g_2_d,  g_3_d, g_4_d,
                                          g_5_d,  g_6_d,  g_7_d,  g_8_d, g_9_d,
                                          g_10_d, g_11_d, g_12_d, g_13_d, 
                                          g_14_d, g_15_d, g_16_d, g_17_d,
                                          g_18_d );

        if( (step%stat) == 0 ){
            cudaThreadSynchronize();
            cudaMemcpy( phi, phi_d, sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
            cudaMemcpy( ux,  ux_d,  sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
            cudaMemcpy( uy,  uy_d,  sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
            cudaMemcpy( uz,  uz_d,  sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
            stats( phi, ux, uy, uz );
        }
        if( (step%save) == 0 ){
            cudaThreadSynchronize();
            cudaMemcpy( phi, phi_d, sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
            cudaMemcpy( rho, rho_d, sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
            cudaMemcpy( ux,  ux_d,  sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
            cudaMemcpy( uy,  uy_d,  sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
            cudaMemcpy( uz,  uz_d,  sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
            vtkSave( nb_east, nb_west, nb_north, nb_south, nb_top, nb_bot, phi, 
                     rho, ux, uy, uz );
        }

    }
    fprintf( stdout, "\nEvolution run completed.\n\n" );
    cudaThreadSynchronize();
    cudaEventRecord( stopMain, 0 );
    cudaEventElapsedTime( &setupTime,   start,     stopSetup );
    cudaEventElapsedTime( &relaxTime,   stopSetup, stopRelax );
    cudaEventElapsedTime( &mainTime,    stopRelax, stopMain );
    cudaEventElapsedTime( &elapsedTime, start,     stopMain );
    fprintf( stdout, "Setup time      : %6.3f ms\n", setupTime );
    fprintf( stdout, "Relaxation time : %6.3f ms\n", relaxTime );
    fprintf( stdout, "Evolution time  : %6.3f ms\n", mainTime );
    fprintf( stdout, "Total time      : %6.3f ms\n", elapsedTime );
    fprintf( stdout, "Time for lb step: %6.3f ms\n", mainTime/( 1.f*maxStep ) );

    // Save final configuration
    cudaMemcpy( phi, phi_d, sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
    cudaMemcpy( rho, rho_d, sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
    cudaMemcpy( ux,  ux_d,  sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
    cudaMemcpy( uy,  uy_d,  sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
    cudaMemcpy( uz,  uz_d,  sizeof(float)*gridSize, cudaMemcpyDeviceToHost );
    logSave( devName, devMem, nb_east, nb_west, nb_north, nb_south, nb_top, 
             nb_bot, phi, rho, ux, uy, uz );
    stats( phi, ux, uy, uz );
    vtkSave( nb_east, nb_west, nb_north, nb_south, nb_top, nb_bot, phi, 
             rho, ux, uy, uz );

//====================== Free host dynamic arrays ==============================
    free( nb_east );   free( nb_west );   free( phi );
    free( nb_north );  free( nb_south );  free( rho );
    free( nb_top );    free( nb_bot );    free( ux );  free( uy );  free( uz );

//====================== Free device dynamic arrays ==============================
    cudaFree( nb_east_d );  cudaFree( nb_west_d );
    cudaFree( nb_north_d ); cudaFree( nb_south_d );
    cudaFree( nb_top_d );   cudaFree( nb_bot_d );

    cudaFree( phi_d ); cudaFree( rho_d ); 
    cudaFree( ux_d );  cudaFree( uy_d );  cudaFree( uz_d );

    cudaFree( f_0_d ); cudaFree( f_1_d ); cudaFree( f_2_d ); cudaFree( f_3_d );
    cudaFree( f_4_d ); cudaFree( f_5_d ); cudaFree( f_6_d );

    cudaFree( g_0_d );  cudaFree( g_1_d );  cudaFree( g_2_d );
    cudaFree( g_3_d );  cudaFree( g_4_d );  cudaFree( g_5_d );
    cudaFree( g_6_d );  cudaFree( g_7_d );  cudaFree( g_8_d );
    cudaFree( g_9_d );  cudaFree( g_10_d ); cudaFree( g_11_d );
    cudaFree( g_12_d ); cudaFree( g_13_d ); cudaFree( g_14_d );
    cudaFree( g_15_d ); cudaFree( g_16_d ); cudaFree( g_17_d ); 
    cudaFree( g_18_d );

    return 0;
}



