//==============================================================================
// File     : collision.c
// Revision : 1.0
// Author   : Carlos Rosales [ carlos.rosales.fernandez (at) gmail.com ]
//==============================================================================
// Collision step for link directions (0-18) of the momentum distribution.
// Includes gravitational force (downward along the z axis). 
//==============================================================================

__global__ void collision_g( int *nb_east_d,  int *nb_west_d, int *nb_north_d, 
                                  int *nb_south_d, int *nb_top_d, int *nb_bot_d,
                                  float *phi_d,  float *rho_d,  float *ux_d,
                                  float *uy_d,   float *uz_d,   float *g_0_d,
                                  float *g_1_d,  float *g_2_d,  float *g_3_d,
                                  float *g_4_d,  float *g_5_d,  float *g_6_d,
                                  float *g_7_d,  float *g_8_d,  float *g_9_d,
                                  float *g_10_d, float *g_11_d, float *g_12_d,
                                  float *g_13_d, float *g_14_d, float *g_15_d,
                                  float *g_16_d, float *g_17_d, float *g_18_d )
{
//==============================================================================
// 23 Registers | 30 arguments
//==============================================================================
    int   i, idx, idx2, ie, iw, j, jn, js, k, kt, kb;
    float phin, muPhi, rhon, temp;
    float lapPhi, Fx, Fy, Fz;
    float uxn, uyn, uzn, UF;
//==============================================================================

    // Identify current thread
    i = blockIdx.x * blockDim.x + threadIdx.x;
    j = blockIdx.y * blockDim.y + threadIdx.y;

    // Main collision loop
    for( k = 0; k < NZ; k++ ){

        // Define some local values
        idx  = gridId( i, j, k );
        idx2 = gridId( i, j, k ) + dcol;
        phin = phi_d[idx];
        rhon = rho_d[idx];

        // Near neighbors
        ie = nb_east_d[idx];
        iw = nb_west_d[idx];
        jn = nb_north_d[idx];
        js = nb_south_d[idx];
        kt = nb_top_d[idx];
        kb = nb_bot_d[idx];

        // Laplacian of the order parameter Phi
        lapPhi = ( phi_d[ gridId(ie,jn,k ) ] + phi_d[ gridId(iw,js,k ) ] 
               +   phi_d[ gridId(ie,js,k ) ] + phi_d[ gridId(iw,jn,k ) ]
               +   phi_d[ gridId(ie,j ,kt) ] + phi_d[ gridId(iw,j ,kb) ]
               +   phi_d[ gridId(ie,j ,kb) ] + phi_d[ gridId(iw,j ,kt) ]
               +   phi_d[ gridId(i ,jn,kt) ] + phi_d[ gridId(i ,js,kb) ]
               +   phi_d[ gridId(i ,jn,kb) ] + phi_d[ gridId(i ,js,kt) ]
               + 2.f*( phi_d[ gridId(ie,j ,k ) ] + phi_d[ gridId(iw,j ,k ) ] 
               +       phi_d[ gridId(i ,jn,k ) ] + phi_d[ gridId(i ,js,k ) ] 
               +       phi_d[ gridId(i ,j ,kt) ] + phi_d[ gridId(i ,j ,kb) ]
               -       12.f*phin ) )*inv6;

        // Force calculation ( +grav only if phi > 0 )
        // Fx = muPhi*gradPhiX
        // Fy = muPhi*gradPhiY
        // Fz = muPhi*gradPhiZ + grav
        muPhi = alpha4_d*phin*( phin*phin - phiStarSq_d ) - kappa_d*lapPhi;

        Fx = muPhi*( 2.f*( phi_d[ gridId(ie,j ,k ) ] - phi_d[ gridId(iw,j ,k ) ] ) 
           + phi_d[ gridId(ie,jn,k ) ] - phi_d[ gridId(iw,js,k ) ] 
           + phi_d[ gridId(ie,js,k ) ] - phi_d[ gridId(iw,jn,k ) ] 
           + phi_d[ gridId(ie,j ,kt) ] - phi_d[ gridId(iw,j ,kb) ]
           + phi_d[ gridId(ie,j ,kb) ] - phi_d[ gridId(iw,j ,kt) ] )*inv12;

        Fy = muPhi*( 2.f*( phi_d[ gridId(i ,jn,k ) ] - phi_d[ gridId(i ,js,k ) ] ) 
           + phi_d[ gridId(ie,jn,k ) ] - phi_d[ gridId(iw,js,k ) ] 
           + phi_d[ gridId(iw,jn,k ) ] - phi_d[ gridId(ie,js,k ) ]
           + phi_d[ gridId(i ,jn,kt) ] - phi_d[ gridId(i ,js,kb) ]
           + phi_d[ gridId(i ,jn,kb) ] - phi_d[ gridId(i ,js,kt) ] )*inv12;

        Fz = muPhi*( 2.f*( phi_d[ gridId(i ,j ,kt) ] - phi_d[ gridId(i ,j ,kb) ] )
           + phi_d[ gridId(ie,j ,kt) ] - phi_d[ gridId(iw,j ,kb) ]
           + phi_d[ gridId(iw,j ,kt) ] - phi_d[ gridId(ie,j ,kb) ]
           + phi_d[ gridId(i ,jn,kt) ] - phi_d[ gridId(i ,js,kb) ]
           + phi_d[ gridId(i ,js,kt) ] - phi_d[ gridId(i ,jn,kb) ] )*inv12;

         if( phin > 0.f ) Fx = Fx + grav_d;

        // Correct velocity calculation to include interfacial 
        // and gravitational forces and scale by invRho
        uxn = ux_d[idx] + 0.5f * Fx / rhon;
        uyn = uy_d[idx] + 0.5f * Fy / rhon;
        uzn = uz_d[idx] + 0.5f * Fz / rhon;

        // Collision for momentum distribution function
        // The factor invTauRho for geq is embedded in Egn
        //temp = rhon + K1*( 3.f*phin*muPhi 
        //     - 1.5f*rhon*( uxn*uxn + uyn*uyn + uzn*uzn ) );
        temp = K1*( 3.f*phin*muPhi + rhon*( 1.f
             - 1.5f*( uxn*uxn + uyn*uyn + uzn*uzn ) ) ); 
        UF   = uxn*Fx + uyn*Fy + uzn*Fz;

        // Rescale velocity to avoid unnecessary product operations
        uxn = uxn*invCs_sq;
        uyn = uyn*invCs_sq;
        uzn = uzn*invCs_sq;

        // The equilibrium g value and the force are bundled into gFs
        // DIRECTION 0
        // The 1/6.f factor is because we have rescaled uxn,uyn,uzn by invCs_Sq
        g_0_d[idx2] = invTauRhoOne_d*g_0_d[idx] 
                    + K0*( rhon - 6.f*phin*muPhi - rhon*( uxn*uxn + uyn*uyn
                    + uzn*uzn )/6.f ) - KC0*UF;

        // DIRECTIONS 1 & 2
        g_1_d[idx2] = invTauRhoOne_d*g_1_d[idx] + temp 
                    + K1*rhon*uxn*( 0.5f*uxn + 1.f )
                    + KC1*( ( uxn + 1.f )*Fx - UF );
        g_2_d[idx2] = invTauRhoOne_d*g_2_d[idx] + temp
                    + K1*rhon*uxn*( 0.5f*uxn - 1.f ) 
                    + KC1*( ( uxn - 1.f )*Fx - UF );

        // DIRECTIONS 3 & 4
        g_3_d[idx2] = invTauRhoOne_d*g_3_d[idx] + temp
                    + K1*rhon*uyn*( 0.5f*uyn + 1.f )
                    + KC1*( ( uyn + 1.f )*Fy - UF );
        g_4_d[idx2] = invTauRhoOne_d*g_4_d[idx] + temp
                    + K1*rhon*uyn*( 0.5f*uyn - 1.f )
                    + KC1*( ( uyn - 1.f )*Fy - UF );

        // DIRECTIONS 5 & 6
        g_5_d[idx2] = invTauRhoOne_d*g_5_d[idx] + temp
                    + K1*rhon*uzn*( 0.5f*uzn + 1.f )
                    + KC1*( ( uzn + 1.f )*Fz - UF );
        g_6_d[idx2] = invTauRhoOne_d*g_6_d[idx] + temp
                    + K1*rhon*uzn*( 0.5f*uzn - 1.f )
                    + KC1*( ( uzn - 1.f )*Fz - UF );


        // Notice we reset "temp" to use the correct K2 constant
        // The 1/6.f factor is because we have rescaled uxn,uyn,uzn by invCs_Sq
        temp = K2*( 3.f*phin*muPhi + rhon*( 1.f 
             - ( uxn*uxn + uyn*uyn + uzn*uzn )/6.f ) );

        // DIRECTION 7 & 8
        g_7_d[idx2] = invTauRhoOne_d*g_7_d[idx] + temp
                      + K2*rhon*( uxn + uyn )*( 0.5f*( uxn + uyn ) + 1.f )
                      + KC2*( ( ( uxn + uyn ) + 1.f )*( Fx + Fy ) - UF );
        g_8_d[idx2] = invTauRhoOne_d*g_8_d[idx] + temp
                      + K2*rhon*( uxn + uyn )*( 0.5f*( uxn + uyn ) - 1.f )
                      + KC2*( ( ( uxn + uyn ) - 1.f )*( Fx + Fy ) - UF );

        // DIRECTIONS 9 & 10
        g_9_d[idx2] = invTauRhoOne_d*g_9_d[idx] + temp 
                    + K2*rhon*( uxn - uyn )*( 0.5f*( uxn - uyn ) + 1.f )  
                    + KC2*( ( ( uxn - uyn ) + 1.f )*( Fx - Fy ) - UF );
        g_10_d[idx2] = invTauRhoOne_d*g_10_d[idx] + temp 
                     + K2*rhon*( uxn - uyn )*( 0.5f*( uxn - uyn ) - 1.f )
                     + KC2*( ( ( uxn - uyn ) - 1.f )*( Fx - Fy ) - UF );

        // DIRECTIONS 11 & 12
        g_11_d[idx2] = invTauRhoOne_d*g_11_d[idx] + temp
                     + K2*rhon*( uxn + uzn )*( 0.5f*( uxn + uzn ) + 1.f )
                     + KC2*( ( ( uxn + uzn ) + 1.f )*( Fx + Fz ) - UF );
        g_12_d[idx2] = invTauRhoOne_d*g_12_d[idx] + temp 
                     + K2*rhon*( uxn + uzn )*( 0.5f*( uxn + uzn ) - 1.f )
                     + KC2*( ( ( uxn + uzn ) - 1.f )*( Fx + Fz ) - UF );

        // DIRECTIONS 13 & 14
        g_13_d[idx2] = invTauRhoOne_d*g_13_d[idx] + temp 
                     + K2*rhon*( uxn - uzn )*( 0.5f*( uxn - uzn ) + 1.f )
                     + KC2*( ( ( uxn - uzn ) + 1.f )*( Fx - Fz ) - UF );
        g_14_d[idx2] = invTauRhoOne_d*g_14_d[idx] + temp 
                     + K2*rhon*( uxn - uzn )*( 0.5f*( uxn - uzn ) - 1.f )
                     + KC2*( ( ( uxn - uzn ) - 1.f )*( Fx - Fz ) - UF );

        // DIRECTIONS 15 & 16
        g_15_d[idx2] = invTauRhoOne_d*g_15_d[idx] + temp
                     + K2*rhon*( uyn + uzn )*( 0.5f*( uyn + uzn ) + 1.f )
                     + KC2*( ( ( uyn + uzn ) + 1.f )*( Fy + Fz ) - UF );
        g_16_d[idx2] = invTauRhoOne_d*g_16_d[idx] + temp
                     + K2*rhon*( uyn + uzn )*( 0.5f*( uyn + uzn ) - 1.f )
                     + KC2*( ( ( uyn + uzn ) - 1.f )*( Fy + Fz ) - UF );

        // DIRECTIONS 17 & 18
        g_17_d[idx2] = invTauRhoOne_d*g_17_d[idx] + temp
                     + K2*rhon*( uyn - uzn )*( 0.5f*( uyn - uzn ) + 1.f )
                     + KC2*( ( ( uyn - uzn ) + 1.f )*( Fy - Fz ) - UF );
        g_18_d[idx2] = invTauRhoOne_d*g_18_d[idx] + temp
                     + K2*rhon*( uyn - uzn )*( 0.5f*( uyn - uzn ) - 1.f )
                     + KC2*( ( ( uyn - uzn ) - 1.f )*( Fy - Fz ) - UF );

    }

}

