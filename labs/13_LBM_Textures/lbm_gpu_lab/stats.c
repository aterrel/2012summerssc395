//==============================================================================
// File     : stats.c
// Revision : 1.0
// Author   : Carlos Rosales [ carlos.rosales.fernandez (at) gmail.com ]
//==============================================================================
// Intermediate simulation results, mostly for sanity checks. Includes average
// and maximum velocity in the domain as well as mass conservation. 
//==============================================================================

int stats( float *phi, float *ux, float *uy, float *uz )
{
    FILE  *fp;
    int   i, idx, j, k;
    float Umax, ux_av, uy_av, uz_av;
    float invVol, Ref, Usq, Vef, volume;

    ux_av = uy_av = uz_av = Umax = volume = 0.f;
    for( i = 0; i < NX_h; i++ ){
        for( j = 0; j < NY_h; j++ ){
            for( k = 0; k < NZ_h; k++ ){
                idx = gridId_h( i, j, k );

                Usq = ux[idx]*ux[idx] + uy[idx]*uy[idx] + uz[idx]*uz[idx];
                if( Usq > Umax ) Umax = Usq;

                if( phi[idx] >= 0.f ){
                    volume = volume + 1.f;
                    ux_av  = ux_av + ux[idx];
                    uy_av  = uy_av + uy[idx];
                    uz_av  = uz_av + uz[idx];
                }
            }
        }
    }
    Umax   = sqrtf( Umax );
    invVol = 1.f/volume;

    if( step == 0 ){
        if( RELAX == 0 ){
            invInitVol = invVol;
            fp = fopen( "stats.out", "w" );
        }
        else{
            invRelaxVol = invVol; 
            fp = fopen( "relax_stats.out", "w" );
        }
        fprintf( fp, "#step\t\tux\t\tuy\t\tuz\t\tUmax\t\tVef\t\tRef\n" );
        fclose( fp );
    }

    ux_av = ux_av*invVol;
    uy_av = uy_av*invVol;
    uz_av = uz_av*invVol;

    Ref = pow( volume*invPi*0.75f, 1.f/3.f);

    if( RELAX == 0 ){
        Vef = volume*invInitVol;
        fp = fopen( "stats.out", "a" );
    }
    else{
        Vef = volume*invRelaxVol;
        fp = fopen( "relax_stats.out", "a" );
    }
    fprintf( fp, "%8d\t%8.6f\t%8.6f\t%8.6f\t%8.6f\t%8.6f\t%8.6f\n", step, ux_av, uy_av, uz_av, Umax, Vef, Ref );
    fclose( fp );

    return 0;
}

