#include <stdio.h>


void bubbleSort(int arr[], int n) {
      bool swapped = true;
      int j = 0;
      int tmp;
      while (swapped) {
            swapped = false;
            j++;
            for (int i = 0; i < n - j; i++) {
                  if (arr[i] > arr[i + 1]) {
                        tmp = arr[i];
                        arr[i] = arr[i + 1];
                        arr[i + 1] = tmp;
                        swapped = true;
                  }
            }
      }
}


int main(void)
{
    int a[10] = {4, 5, 1, 9, 10, 6, 3, 8, 7, 2}; 

    printf("in: [ ");
    for (int i=0; i<10; ++i)
	printf("%d ", a[i]);
    printf("]\n");

    bubbleSort(a, 10);
    printf("out: [ ");
    for (int i=0; i<10; ++i)
	printf("%d ", a[i]);
    printf("]\n");
}
