#include <cuda.h>
#include <stdio.h>

#define N 64

__global__ void bubble( volatile int *a ) {
    __shared__ unsigned int more;
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    int i = 2*tid;
 
    if (tid == 0)
        more = 1; // more work?

    while (more)
    {
        if (tid == 0) more = 0;
        
        if (a[i] > a[i+1])
        {
            int temp = a[i];
            a[i] = a[i+1];
            a[i+1] = temp;
            atomicAdd(&more,1);
        }
        __syncthreads();
        if (tid > 0 && a[i-1] > a[i]) 
        {
            int temp = a[i];
            a[i] = a[i-1];
            a[i-1] = temp;
            atomicAdd(&more,1);
        }
        __syncthreads();
    }
}

int main( void ) {

    int *dev_a, *a = new int[N];

    for (int i=0; i<N; ++i)
    {
        a[i] = int(1024.f * rand()/(float)RAND_MAX);
        printf("%d ", a[i]);
    }
    printf("\n\n");

    // allocate the memory on the GPU
    cudaMalloc( (void**)&dev_a, N * sizeof(int) ) ;

    // copy the arrays 'a' and 'b' to the GPU
    cudaMemcpy( dev_a, a, N * sizeof(int),
                              cudaMemcpyHostToDevice ) ;

    bubble<<<1,N/2>>>( dev_a );

    // copy the array back from the GPU to the CPU
    cudaMemcpy( a, dev_a, N * sizeof(int),
                              cudaMemcpyDeviceToHost ) ;

    // verify that the GPU did the work we requested
    for (int i=0; i<N; ++i)
        printf("%d ",a[i]);
    printf("\n");
   
    // free the memory we allocated on the GPU
    cudaFree( dev_a ) ;

    // free the memory we allocated on the CPU
    delete[] a;

    return 0;
}

