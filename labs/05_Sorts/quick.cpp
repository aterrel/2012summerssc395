#include <stdio.h>

void quickSort(int arr[], int left, int right) {
      int i = left, j = right;
      int tmp;
      int pivot = arr[(left + right) / 2];
 
      /* partition */
      while (i <= j) {
            while (arr[i] < pivot)
                  i++;
            while (arr[j] > pivot)
                  j--;
            if (i <= j) {
                  tmp = arr[i];
                  arr[i] = arr[j];
                  arr[j] = tmp;
                  i++;
                  j--;
            }
      };
 
      /* recursion */
      if (left < j)
            quickSort(arr, left, j);
      if (i < right)
            quickSort(arr, i, right);
}

int main(void)
{
    int a[10] = {4, 5, 1, 9, 10, 6, 3, 8, 7, 2}; 

    printf("in: [ ");
    for (int i=0; i<10; ++i)
	printf("%d ", a[i]);
    printf("]\n");

    quickSort(a, 0, 9);
    printf("out: [ ");
    for (int i=0; i<10; ++i)
	printf("%d ", a[i]);
    printf("]\n");
}

