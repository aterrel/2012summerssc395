#include <stdio.h>

// m - size of A
// n - size of B
// size of C array must be equal or greater than
// m + n
void merge(int m, int n, int A[], int B[], int C[]) {
      int i, j, k;
      i = 0;
      j = 0;
      k = 0;
      while (i < m && j < n) {
            if (A[i] <= B[j]) {
                  C[k] = A[i];
                  i++;
            } else {
                  C[k] = B[j];
                  j++;
            }
            k++;
      }
      if (i < m) {
            for (int p = i; p < m; p++) {
                  C[k] = A[p];
                  k++;
            }
      } else {
            for (int p = j; p < n; p++) {
                  C[k] = B[p];
                  k++;
            }
      }
}

int main(void)
{
    int a[10] = {1,3,5,7,9,11,13,15,17,19};
    int b[10] = {2,4,6,8,10,12,14,16,18,20};
    int c[20];

    printf("a: [ ");
    for (int i=0; i<10; ++i)
	printf("%d ", a[i]);
    printf("]\n");

    printf("b: [ ");
    for (int i=0; i<10; ++i)
	printf("%d ", b[i]);
    printf("]\n");

    merge(10,10,a,b,c);
    printf("c: [ ");
    for (int i=0; i<20; ++i)
	printf("%d ", c[i]);
    printf("]\n");
}
