/*
 * All MDH kernels originally developed by David Gohara.
 * Extended to multiple platforms, device types, and vector
 * widths by John Stone.
 *
 * Questions about this code should be directed to
 *   David Gohara <sdg0919@gmail.com>
 *   John Stone <johns@ks.uiuc.edu>
 *
 * This kernel is in the public domain.
 *
 */

typedef float4 floatvec;
__kernel 
#if defined(WGSIZE)
__attribute__((reqd_work_group_size(WGSIZE, 1, 1)))
#endif
void mdh(  __global float *ax, __global float *ay, __global float *az,
                    __global float *charge, __global float *size, 
                    __global floatvec *gx,  
                    __global floatvec *gy, 
                    __global floatvec *gz,
                    float pre1, float xkappa, __global floatvec *val, int natoms, 
                    __local float * shared )
{
   int igrid = get_global_id(0);
   int lsize = get_local_size(0);
   int lid = get_local_id(0);
   floatvec v = 0.0f;
   
   floatvec lgx = gx[igrid];
   floatvec lgy = gy[igrid];
   floatvec lgz = gz[igrid];
   
   for(int jatom = 0; jatom < natoms; jatom+=lsize )
   {
        if((jatom+lsize) > natoms) 
            lsize = natoms - jatom;
        
        if((jatom + lid) < natoms){
            shared[lid          ] = ax[jatom + lid];
            shared[lid +   lsize] = ay[jatom + lid];
            shared[lid + 2*lsize] = az[jatom + lid];
            shared[lid + 3*lsize] = charge[jatom + lid];
            shared[lid + 4*lsize] = size[jatom + lid];
        }
        barrier(CLK_LOCAL_MEM_FENCE);
        
        for(int i=0; i<lsize; i++) {
#if defined(SHAREDMEMNOBCAST)
             // AMD/ATI GPUs take a big perf hit if all
             // threads read from the same local memory cell due to
             // bank conflicts.  The workaround is to cause each thread
             // to read from a different lane, with wraparound.

             // lsize must be an exact power of 2
             int limod = (i + lid) & (lsize - 1);
             floatvec dx = lgx - shared[limod          ];
             floatvec dy = lgy - shared[limod +   lsize];
             floatvec dz = lgz - shared[limod + 2*lsize];
             floatvec dist = sqrt(dx*dx + dy*dy + dz*dz);
             v += pre1 * (shared[limod + 3*lsize] / dist)  *
                  exp( -xkappa * (dist - shared[limod + 4*lsize])) /
                  (1.0f + xkappa * shared[limod + 4*lsize]);
#else
             floatvec dx = lgx - shared[i          ];
             floatvec dy = lgy - shared[i +   lsize];
             floatvec dz = lgz - shared[i + 2*lsize];
             floatvec dist = sqrt( dx * dx + dy * dy + dz * dz );
             v += pre1 * ( shared[i + 3*lsize] / dist )  *
                  exp( -xkappa * (dist - shared[i + 4*lsize])) /
                  (1.0f + xkappa * shared[i + 4*lsize]);
#endif
        }
        barrier(CLK_LOCAL_MEM_FENCE);
   }

   val[ igrid ] = v;
}
