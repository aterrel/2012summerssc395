/*
 * A set of simple Multiple Debye-Huckel (MDH) kernels 
 * inspired by APBS:
 *   http://www.poissonboltzmann.org/ 
 *
 * This code was all originally written by David Gohara on MacOS X, 
 * and has been subsequently been modified by John Stone, porting to Linux,
 * adding vectorization, and several other platform-specific 
 * performance optimizations.
 * 
 */

#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#if defined(__APPLE__)
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#include "WKFThreads.h"
#include "WKFUtils.h"

#if 1
#if defined(_MSC_VER)
#define CLERR \
  if (err != CL_SUCCESS) {                     \
    printf("opencl error %d, line %d\n", err, __LINE__); \
    printf("Press any key to exit test...\n"); \
    getc(stdin); \
    exit(-1);                                   \
  }
#else
#define CLERR \
  if (err != CL_SUCCESS) {                     \
    printf("opencl error %d, line %d\n", err, __LINE__); \
    exit(-1);                                   \
  }
#endif
#else
#define CLERR
#endif

#define SEP printf("-----------------------------------------------------------\n")

int device_stats(cl_device_id device_id) {
    int err;
    size_t returned_size;
    int i;
 
    // Report the device vendor and device name
    // 
    cl_char vendor_name[1024] = {0};
    cl_char device_name[1024] = {0};
    cl_char device_profile[1024] = {0};
    cl_char device_extensions[1024] = {0};
    cl_device_local_mem_type local_mem_type;
    
    cl_ulong global_mem_size, global_mem_cache_size;
    cl_ulong max_mem_alloc_size;
    
    cl_uint clock_frequency, vector_width, max_compute_units;
    
    size_t max_work_item_dims,max_work_group_size, max_work_item_sizes[3];
    
    cl_uint vector_types[] = {CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR, CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT, CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT,CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG,CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT,CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE}; 
    char *vector_type_names[] = {"char","short","int","long","float","double"};
    
    err = clGetDeviceInfo(device_id, CL_DEVICE_VENDOR, sizeof(vendor_name), vendor_name, &returned_size);
    err|= clGetDeviceInfo(device_id, CL_DEVICE_NAME, sizeof(device_name), device_name, &returned_size);
    err|= clGetDeviceInfo(device_id, CL_DEVICE_PROFILE, sizeof(device_profile), device_profile, &returned_size);
    err|= clGetDeviceInfo(device_id, CL_DEVICE_EXTENSIONS, sizeof(device_extensions), device_extensions, &returned_size);
    err|= clGetDeviceInfo(device_id, CL_DEVICE_LOCAL_MEM_TYPE, sizeof(local_mem_type), &local_mem_type, &returned_size);
    
    err|= clGetDeviceInfo(device_id, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(global_mem_size), &global_mem_size, &returned_size);
    err|= clGetDeviceInfo(device_id, CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE, sizeof(global_mem_cache_size), &global_mem_cache_size, &returned_size);
    err|= clGetDeviceInfo(device_id, CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(max_mem_alloc_size), &max_mem_alloc_size, &returned_size);
    
    err|= clGetDeviceInfo(device_id, CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(clock_frequency), &clock_frequency, &returned_size);
    
    err|= clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(max_work_group_size), &max_work_group_size, &returned_size);
    
    err|= clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(max_work_item_dims), &max_work_item_dims, &returned_size);
    
    err|= clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(max_work_item_sizes), max_work_item_sizes, &returned_size);
    
    err|= clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(max_compute_units), &max_compute_units, &returned_size);
    
    printf("Vendor: %s\n", vendor_name);
    printf("Device Name: %s\n", device_name);
    printf("Profile: %s\n", device_profile);
    printf("Supported Extensions: %s\n\n", device_extensions);
    
    printf("Local Mem Type (Local=1, Global=2): %i\n",(int)local_mem_type);
    printf("Global Mem Size (MB): %i\n",(int)((float)global_mem_size/(1024.0*1024.0)));
    printf("Global Mem Cache Size (Bytes): %i\n",(int)global_mem_cache_size);
    printf("Max Mem Alloc Size (MB): %ld\n",(long int)max_mem_alloc_size/(1024*1024));
    
    printf("Clock Frequency (MHz): %i\n\n",clock_frequency);
    
    for(i=0;i<6;i++){
        err|= clGetDeviceInfo(device_id, vector_types[i], sizeof(clock_frequency), &vector_width, &returned_size);
        printf("Vector type width for: %s = %i\n",vector_type_names[i],vector_width);
    }
    
    printf("\nMax Work Group Size: %lu\n",max_work_group_size);
    //printf("Max Work Item Dims: %lu\n",max_work_item_dims);
    //for(size_t i=0;i<max_work_item_dims;i++) 
    //    printf("Max Work Items in Dim %lu: %lu\n",(long unsigned)(i+1),(long unsigned)max_work_item_sizes[i]);
    
    printf("Max Compute Units: %i\n",max_compute_units);
    printf("\n");
    
    return CL_SUCCESS;
}

char * load_program_source(const char *filename)
{ 
    
    struct stat statbuf;
    FILE *fh; 
    char *source; 
    
    fh = fopen(filename, "r");
    if (fh == 0)
        return 0; 
    
    stat(filename, &statbuf);
    source = (char *) malloc(statbuf.st_size + 1);
    fread(source, statbuf.st_size, 1, fh);
    source[statbuf.st_size] = '\0'; 
    
    return source; 
} 


void readfile(float *ax,float *ay,float *az,
              float *gx,float *gy,float *gz,
              float *charge,float *size,int natom,int ngrid){
    
    int i;    
    
    FILE * pFile = NULL;
    pFile = fopen("atom.txt","r");
    
    printf("Reading input file 1\n");
    for (i=0; i<natom; i++) {
        
        fscanf(pFile,"%g %g %g %g %g",
               &ax[i],&ay[i],&az[i],&charge[i],&size[i]);
    }
    fclose(pFile);
    
    pFile = fopen("grid.txt","r");
    
    printf("Reading input file 2\n");
    for (i=0; i<ngrid; i++) {
        
        fscanf(pFile,"%g %g %g",
               &gx[i],&gy[i],&gz[i]);
    }
    fclose(pFile);
    
    printf("Done reading inputs.");
}

void gendata(float *ax,float *ay,float *az,
             float *gx,float *gy,float *gz,
             float *charge,float *size,int natom,int ngrid){
    
    int i;    
    
    printf("Generating Data 1\n");
    for (i=0; i<natom; i++) {
        
        ax[i] = ((float) rand() / (float) RAND_MAX);
        ay[i] = ((float) rand() / (float) RAND_MAX);
        az[i] = ((float) rand() / (float) RAND_MAX);
        charge[i] = ((float) rand() / (float) RAND_MAX);
        size[i] = ((float) rand() / (float) RAND_MAX);
    }
    
    printf("Generating Data 2\n");
    for (i=0; i<ngrid; i++) {
        
        gx[i] = ((float) rand() / (float) RAND_MAX);
        gy[i] = ((float) rand() / (float) RAND_MAX);
        gz[i] = ((float) rand() / (float) RAND_MAX);
        
    }
    printf("Done generating inputs.\n\n");
}

void print_total(float * arr, int ngrid){
    int i;
    double accum = 0.0;
    for (i=0; i<ngrid; i++){
        accum += arr[i];
    }
    printf("Accumulated value: %1.7g\n",accum);
}

int exec_kernel(cl_device_type devtype, int wgsize, int vecsize, 
                int ngrid, cl_int natom, int ngadj, int naadj,
                float *ax, float *ay, float *az,
                float *gx, float *gy, float *gz,
                float *charge, float *size, 
                cl_float xkappa, cl_float pre1, 
                float *val, int itmax,
                const char * filename, const char *compileflags,
                double *runtotal){
    int i;
    cl_context        context;
    cl_command_queue  cmd_queue;
    cl_device_id      devices;
    cl_int            err;
    double cl_attach, cl_compile, cl_alloc, cl_enqueue, cl_read, cl_total;
    cl_platform_id platforms[16];
    cl_uint num_platforms; 
    size_t returned_size = 0;
    size_t max_workgroup_size = 0;
    cl_char vendor_name[1024] = {0};
    cl_char device_name[1024] = {0};
  
    wkf_timerhandle timer = wkf_timer_create();
    wkf_timerhandle totaltimer = wkf_timer_create();

    wkf_timer_start(timer);
    err = clGetPlatformIDs(16, platforms, &num_platforms);
    err = clGetPlatformIDs(num_platforms, platforms, NULL);
    
    // Connect to a compute device
    //
    err = clGetDeviceIDs(platforms[0], devtype, 1, &devices, NULL);
   
    err = clGetDeviceInfo(devices, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(max_workgroup_size), &max_workgroup_size, &returned_size);
   
    err = clGetDeviceInfo(devices, CL_DEVICE_VENDOR, sizeof(vendor_name), vendor_name, &returned_size);
    err|= clGetDeviceInfo(devices, CL_DEVICE_NAME, sizeof(device_name), device_name, &returned_size);
    
    printf("Connecting to %s %s...\n", vendor_name, device_name);
//    device_stats(devices);
    
    // Read the program
    printf("Loading program '%s'\n", filename);
    char *program_source = load_program_source(filename);

    //Create the context and command queue
    context = clCreateContext(0, 1, &devices, NULL, NULL, &err);
    cmd_queue = clCreateCommandQueue(context, devices, 0, NULL);

    wkf_timer_stop(timer); 
    cl_attach=wkf_timer_time(timer);
    
    //Allocate memory for programs and kernels
    cl_program program;
    cl_kernel kernel;
    
    wkf_timer_start(timer); 
    //Create program from .cl file
    program = clCreateProgramWithSource(context, 1, (const char**)&program_source, NULL, &err);

#if defined(__powerpc)
    // IBM Cell/PowerPC OpenCL compiler needs the workgroup size hard-coded
    // in to the kernel code, so we use macros to do this for us.
    char fullcompflags[4096];
    sprintf(fullcompflags, "%s -DWGSIZE=%d", compileflags, wgsize);
    
    // build the program (compile it)
    err = clBuildProgram(program, 0, NULL, fullcompflags, NULL, NULL);
#else
    // build the program (compile it)
    err = clBuildProgram(program, 0, NULL, compileflags, NULL, NULL);
#endif
    CLERR

#if 0
    char build[2048] = {0};
    clGetProgramBuildInfo(program, devices, CL_PROGRAM_BUILD_LOG, sizeof(build), build, NULL);
    printf("Build Log:\n%s\n",build);
#endif
    
    // create the kernel 
    kernel = clCreateKernel(program, "mdh", &err);
    
    size_t atom_buffer_size = sizeof(float) * natom;
    size_t grid_buffer_size = sizeof(float) * ngrid;
    size_t gadj_buffer_size = sizeof(float) * ngadj;

    wkf_timer_stop(timer); 
    cl_compile = wkf_timer_time(timer);
   
    wkf_timer_start(totaltimer);
    wkf_timer_start(timer); 

    //Allocate memory and queue it to be written to the device
    cl_mem ax_mem = clCreateBuffer(context, CL_MEM_READ_ONLY, atom_buffer_size, NULL, NULL);
    err = clEnqueueWriteBuffer(cmd_queue, ax_mem, CL_TRUE, 0, atom_buffer_size, (void*)ax, 0, NULL, NULL);
    
    cl_mem ay_mem = clCreateBuffer(context, CL_MEM_READ_ONLY, atom_buffer_size, NULL, NULL);
    err = clEnqueueWriteBuffer(cmd_queue, ay_mem, CL_TRUE, 0, atom_buffer_size, (void*)ay, 0, NULL, NULL);
    
    cl_mem az_mem = clCreateBuffer(context, CL_MEM_READ_ONLY, atom_buffer_size, NULL, NULL);
    err = clEnqueueWriteBuffer(cmd_queue, az_mem, CL_TRUE, 0, atom_buffer_size, (void*)az, 0, NULL, NULL);
    
    cl_mem charge_mem = clCreateBuffer(context, CL_MEM_READ_ONLY, atom_buffer_size, NULL, NULL);
    err = clEnqueueWriteBuffer(cmd_queue, charge_mem, CL_TRUE, 0, atom_buffer_size, (void*)charge, 0, NULL, NULL);
    
    cl_mem size_mem    = clCreateBuffer(context, CL_MEM_READ_ONLY, atom_buffer_size, NULL, NULL);
    err = clEnqueueWriteBuffer(cmd_queue, size_mem, CL_TRUE, 0, atom_buffer_size, (void*)size, 0, NULL, NULL);
    
    cl_mem gx_mem = clCreateBuffer(context, CL_MEM_READ_ONLY, gadj_buffer_size, NULL, NULL);
    err = clEnqueueWriteBuffer(cmd_queue, gx_mem, CL_TRUE, 0, gadj_buffer_size, (void*)gx, 0, NULL, NULL);
    
    cl_mem gy_mem = clCreateBuffer(context, CL_MEM_READ_ONLY, gadj_buffer_size, NULL, NULL);
    err = clEnqueueWriteBuffer(cmd_queue, gy_mem, CL_TRUE, 0, gadj_buffer_size, (void*)gy, 0, NULL, NULL);
    
    cl_mem gz_mem = clCreateBuffer(context, CL_MEM_READ_ONLY, gadj_buffer_size, NULL, NULL);
    err = clEnqueueWriteBuffer(cmd_queue, gz_mem, CL_TRUE, 0, gadj_buffer_size, (void*)gz, 0, NULL, NULL);
    
    cl_mem val_mem = clCreateBuffer(context, CL_MEM_READ_WRITE, gadj_buffer_size, NULL, NULL);
    err = clEnqueueWriteBuffer(cmd_queue, val_mem, CL_TRUE, 0, gadj_buffer_size, (void*)val, 0, NULL, NULL);
    
    //Push the data out to device
    clFinish(cmd_queue);
   
    wkf_timer_stop(timer); 
    cl_alloc = wkf_timer_time(timer);
    
    // set work-item dimensions 
    size_t global_work_size, local_work_size, shared_size;

    // scale number of work units by vector size
    global_work_size = ngadj / vecsize;
    local_work_size = wgsize;
    shared_size = (5 * local_work_size) * sizeof(cl_float);
    
    //Set kernel arguments
    err  = clSetKernelArg(kernel,  0, sizeof(cl_mem), &ax_mem);
    err |= clSetKernelArg(kernel,  1, sizeof(cl_mem), &ay_mem);
    err |= clSetKernelArg(kernel,  2, sizeof(cl_mem), &az_mem);
    err |= clSetKernelArg(kernel,  3, sizeof(cl_mem), &charge_mem);
    err |= clSetKernelArg(kernel,  4, sizeof(cl_mem), &size_mem);
    err |= clSetKernelArg(kernel,  5, sizeof(cl_mem), &gx_mem);
    err |= clSetKernelArg(kernel,  6, sizeof(cl_mem), &gy_mem);
    err |= clSetKernelArg(kernel,  7, sizeof(cl_mem), &gz_mem);
    err |= clSetKernelArg(kernel,  8, sizeof(cl_float), &pre1);
    err |= clSetKernelArg(kernel,  9, sizeof(cl_float), &xkappa);
    err |= clSetKernelArg(kernel, 10, sizeof(cl_mem), &val_mem);
    err |= clSetKernelArg(kernel, 11, sizeof(cl_int), &natom);
    err |= clSetKernelArg(kernel, 12, shared_size, NULL);
    
    size_t thread_size;
    clGetKernelWorkGroupInfo(kernel,devices,CL_KERNEL_WORK_GROUP_SIZE,sizeof(thread_size),&thread_size,NULL);
//    printf("Recommended Size: %lu\n",thread_size);
   
    wkf_timer_start(timer);
    //Queue up the kernels itmax times
    for(i=0;i<itmax;i++) {
        err = clEnqueueNDRangeKernel(cmd_queue, kernel, 1, NULL, &global_work_size, 
                                     &local_work_size, 0, NULL, NULL);
        CLERR
    } 
    //Finish the calculation
    clFinish(cmd_queue);
    wkf_timer_stop(timer);
    // report average kernel runtime
    cl_enqueue = wkf_timer_time(timer) / ((double) itmax);
    
    wkf_timer_start(timer);
    // read output image
    err = clEnqueueReadBuffer(cmd_queue, val_mem, CL_TRUE, 0, grid_buffer_size, val, 0, NULL, NULL);
    clFinish(cmd_queue);
    wkf_timer_stop(timer);
    cl_read = wkf_timer_time(timer);

    wkf_timer_stop(totaltimer);
    cl_total = wkf_timer_time(totaltimer);
 
    printf("Attach: %.2f Compile: %.2f Alloc: %1.12g Enqueue: %1.12g Read: %1.12g\n",
           cl_attach, cl_compile, cl_alloc, cl_enqueue, cl_read);
    print_total(val, ngrid);

    // release kernel, program, and memory objects
    clReleaseKernel(kernel);
    clReleaseProgram(program);
    clReleaseCommandQueue(cmd_queue);
    clReleaseContext(context);
    
    clReleaseMemObject(ax_mem);
    clReleaseMemObject(ay_mem);
    clReleaseMemObject(az_mem);
    clReleaseMemObject(charge_mem);
    clReleaseMemObject(size_mem);
    
    clReleaseMemObject(gx_mem);
    clReleaseMemObject(gy_mem);
    clReleaseMemObject(gz_mem);
    
    clReleaseMemObject(val_mem);

    wkf_timer_destroy(timer);
    wkf_timer_destroy(totaltimer);

    *runtotal = cl_total; // return total GPU runtime
    
    return CL_SUCCESS;
}


// reference CPU kernel
void scalar(int ngrid,int natom,
            float *ax,float *ay,float *az,
            float *gx,float *gy,float *gz,
            float *charge,float *size,
            float xkappa,float pre1,
            float *val){
    
    int igrid, iatom;
    
    float dist;
    //#pragma omp parallel for private(igrid,iatom,dist)
    for(igrid=0;igrid<ngrid;igrid++){
        for(iatom=0; iatom<natom; iatom++){
            dist = sqrtf((gx[igrid]-ax[iatom])*(gx[igrid]-ax[iatom]) + 
                         (gy[igrid]-ay[iatom])*(gy[igrid]-ay[iatom]) + 
                         (gz[igrid]-az[iatom])*(gz[igrid]-az[iatom]));
            
            val[igrid] += pre1*(charge[iatom]/dist)*expf(-xkappa*(dist-size[iatom]))
            / (1+xkappa*size[iatom]);
        }
    }
}


void usage() {
  printf("command line parameters:\n");
  printf("Device selection: -cpu -clcpu -clgpu -claccel\n");
  printf("Optional test flags:\n");
  printf("  -itmax N         loop test N times\n");
  printf("  -wgsize          set workgroup size\n");
  printf("  -vecscan         loop over various floatX vector widths\n");
  printf("  -asyncwgcopy     use async_workgroup_copy() for shared mem I/O\n");
  printf("  -testsmemnobcast use ATI alternative shared mem access pattern\n");
}

void getargs(int argc, const char **argv, int *itmax, 
             int *runcpu, int *runclcpu, int *runclgpu, int *runclaccel,
             int *vecscan, int *testsmemnobcast, int *asyncwgcopy,
             int *wgsize) {
  int i;
  for (i=0; i<argc; i++) {
    if ((!strcmp(argv[i], "-itmax")) && ((i+1) < argc)) {
      i++;
      *itmax = atoi(argv[i]);;
    }

    if (!strcmp(argv[i], "-cpu"))
      *runcpu = 1;

    if (!strcmp(argv[i], "-clcpu"))
      *runclcpu = 1;

    if (!strcmp(argv[i], "-clgpu"))
      *runclgpu = 1;

    if (!strcmp(argv[i], "-claccel"))
      *runclaccel = 1;

    if (!strcmp(argv[i], "-vecscan"))
      *vecscan = 1;

    if (!strcmp(argv[i], "-testsmemnobcast"))
      *testsmemnobcast = 1;

    if (!strcmp(argv[i], "-asyncwgcopy"))
      *asyncwgcopy = 1;

    if ((!strcmp(argv[i], "-wgsize")) && ((i+1) < argc)) {
      i++;
      *wgsize = atoi(argv[i]);;
    }

    if (!strcmp(argv[i], "-all")) {
      *runcpu = 1;
      *runclcpu = 1;
      *runclgpu = 1;
      *runclaccel = 1;
      *vecscan = 1;
      *testsmemnobcast = 1;
    }
  }

  printf("Running tests on:\n");
  printf("  CPU: %s\n", (*runcpu) ? "enabled" : "disabled");
  printf("  OpenCL CPU: %s\n", (*runclcpu) ? "enabled" : "disabled");
  printf("  OpenCL GPU: %s\n", (*runclgpu) ? "enabled" : "disabled");
  printf("  OpenCL Accelerator: %s\n", (*runclaccel) ? "enabled" : "disabled");
  printf("\n");
  printf("Run parameters:\n");
  printf("  kernel loop count: %d\n", *itmax);
  printf("     workgroup size: %d\n", *wgsize);
  printf("   vector type scan: %s\n", (*vecscan) ? "enabled" : "disabled");
  printf("  test smem nobcast: %s\n", (*testsmemnobcast) ? "enabled" : "disabled");
  printf("      async wg copy: %s\n", (*asyncwgcopy) ? "enabled" : "disabled");
}


int main(int argc, const char **argv) {
    char compileflags[4096]; /* OpenCL compilation flags */
    double runtotal;
    int naadj = 0;
    int smemnobcast, type;
    int itmax = 1;
    int runcpu = 0;
    int runclcpu = 0;
    int runclgpu = 0;
    int runclaccel = 0;
    int vecscan = 0;
    int testsmemnobcast = 0;
    int asyncwgcopy = 0;
    int wgsize = 64;

    getargs(argc, argv, &itmax, 
            &runcpu, &runclcpu, &runclgpu, &runclaccel,
            &vecscan, &testsmemnobcast, &asyncwgcopy,
            &wgsize);

    if (!(runcpu || runclcpu || runclgpu || runclaccel)) {
      usage();

      printf("\nNo device type selected!  Exiting\n");
      return -1;
    }

    wkf_timerhandle timer = wkf_timer_create();
 
    int natom = 5877;
    int ngrid = 134918;
    int ngadj = ngrid + (512 - (ngrid & 511));
    
    float pre1 = 4.46184985145e19;
    float xkappa = 0.0735516324639;
    
    float *ax = (float*)calloc(natom, sizeof(float));
    float *ay = (float*)calloc(natom, sizeof(float));
    float *az = (float*)calloc(natom, sizeof(float));
    float *charge = (float*)calloc(natom, sizeof(float));
    float *size = (float*)calloc(natom, sizeof(float));

    float *gx = (float*)calloc(ngadj, sizeof(float));
    float *gy = (float*)calloc(ngadj, sizeof(float));
    float *gz = (float*)calloc(ngadj, sizeof(float));
    
    float *val1 = (float*)calloc(ngadj, sizeof(float));
    float *val2 = (float*)calloc(ngadj, sizeof(float));
    
    gendata(ax, ay, az, gx, gy, gz, charge, size, natom, ngrid);
   
    if (runcpu) { 
      wkf_timer_start(timer);
      scalar(ngadj, natom, ax, ay, az, gx, gy, gz, charge, size, xkappa, pre1, val1);
      wkf_timer_stop(timer);

      SEP;
      print_total(val1, ngrid);
      printf("CPU Loop: %1.12g\n", wkf_timer_time(timer));
      SEP;
    }

    for (type=0; type<=2; type++) {
      cl_device_type devtype;

      switch (type) {
        case 0: 
          if (runclcpu)
            devtype = CL_DEVICE_TYPE_CPU;
          else 
            continue;
          break;

        case 2: 
          if (runclaccel)
            devtype = CL_DEVICE_TYPE_ACCELERATOR;
          else 
            continue;
          break;

        case 1:
        default:
          if (runclgpu)
            devtype = CL_DEVICE_TYPE_GPU;
          else 
            continue;
          break;
      }


      SEP;
      exec_kernel(devtype, wgsize, 1, ngrid, natom, ngadj, naadj, 
                  ax, ay, az, gx, gy, gz, 
                  charge, size, xkappa, pre1, val2, itmax, "mdh_orig.cl", "",
                  &runtotal);
      printf("GPU Total - Scalar Unoptimized: %1.12g\n", runtotal);
      SEP;
      
#if 0
      /* fixed scalar kernel */
      SEP;
      wkf_timer_start(timer);
      exec_kernel(devtype, wgsize, 1, ngrid, natom, ngadj, naadj, 
                  ax, ay, az, gx, gy, gz, 
                  charge, size, xkappa, pre1, val2, itmax, "mdh_opt.cl", "",
                  &runtotal);
      wkf_timer_stop(timer);
      printf("GPU Total - Scalar Optimized: %1.12g\n", runtotal);
      SEP;

      /* fixed 4-element vectors */
      SEP;
      wkf_timer_start(timer);
      exec_kernel(devtype, wgsize, 4, ngrid, natom, ngadj, naadj, 
                  ax, ay, az, gx, gy, gz, 
                  charge, size, xkappa, pre1, val2, itmax, "mdh_vec4.cl", "",
                  &runtotal);
      wkf_timer_stop(timer);
      printf("GPU Total - Vec4 Optimized: %1.12g\n", runtotal);
      SEP;
#endif

      /*
       * Dynamically generated vectorized code
       */
      for (smemnobcast=0; smemnobcast <= testsmemnobcast; smemnobcast++) {
        int vecwidth;
        int maxvec=1;
        if (vecscan)
          maxvec=16;
        for (vecwidth=1; vecwidth <= maxvec; vecwidth *= 2) {
          SEP;
          const char *wgcopyflag = (asyncwgcopy) ? "-DASYNCWGCOPY=1" : "";
          const char *smemflag = (smemnobcast) ? "-DSHAREDMEMNOBCAST=1" : "";
          if (vecwidth == 1)
            sprintf(compileflags, "-DFLOATVEC=float %s %s", smemflag, wgcopyflag);
          else
            sprintf(compileflags, "-DFLOATVEC=float%d %s", vecwidth, smemflag, wgcopyflag);
          wkf_timer_start(timer);
          exec_kernel(devtype, wgsize, vecwidth, ngrid, natom, ngadj, naadj, 
                      ax, ay, az, gx, gy, gz, 
                      charge, size, xkappa, pre1, val2, itmax, 
                      "mdh_vecgeneric.cl", 
                      compileflags, &runtotal);
          wkf_timer_stop(timer);
          printf("GPU Total - %s Vector size %d: %1.12g\n", smemflag, vecwidth, runtotal);
          SEP;
        }
      }
    }
    
    free(ax);
    free(ay);
    free(az);
    free(charge);
    free(size);
    
    free(gx);
    free(gy);
    free(gz);
    
    free(val1);
    free(val2);

    wkf_timer_destroy(timer);

#if defined(_MSC_VER)
    // wait for keypress before exiting
    printf("Press any key to exit test...\n");
    int x = getc(stdin);
#endif
    
    return 0;
}



















