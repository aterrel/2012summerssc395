/*
 * All MDH kernels originally developed by David Gohara.
 * Extended to multiple platforms, device types, and vector 
 * widths by John Stone.
 *
 * Questions about this code should be directed to 
 *   David Gohara <sdg0919@gmail.com>
 *   John Stone <johns@ks.uiuc.edu>
 *
 * This kernel is in the public domain. 
 * 
 */

__kernel 
#if defined(WGSIZE)
__attribute__((reqd_work_group_size(WGSIZE, 1, 1)))
#endif
void mdh(  __global float *ax, __global float *ay, __global float *az,
                    __global float *charge, __global float *size, 
                    __global float *gx,  __global float *gy, __global float *gz,
                    float pre1, float xkappa, __global float *val, int natoms, 
                    __local float * shared )
{
   int igrid = get_global_id(0);
   int lsize = get_local_size(0);
   int lid = get_local_id(0);
   float v = 0.0f;
   
   float lgx = gx[igrid];
   float lgy = gy[igrid];
   float lgz = gz[igrid];
   
   for(int jatom = 0; jatom < natoms; jatom+=lsize )
   {
        if((jatom+lsize) > natoms) 
            lsize = natoms - jatom;
        
        if((jatom + lid) < natoms){
            shared[lid          ] = ax[jatom + lid];
            shared[lid +   lsize] = ay[jatom + lid];
            shared[lid + 2*lsize] = az[jatom + lid];
            shared[lid + 3*lsize] = charge[jatom + lid];
            shared[lid + 4*lsize] = size[jatom + lid];
        }
        barrier(CLK_LOCAL_MEM_FENCE);
        
        for(int i=0;i<lsize;i++){
             float dx = lgx - shared[i];
             float dy = lgy - shared[i + lsize];
             float dz = lgz - shared[i + 2*lsize];
    
             float dist = sqrt( dx * dx + dy * dy + dz * dz );
             v += pre1 * ( shared[i + 3*lsize] / dist )  * 
                  exp( -xkappa * (dist - shared[i + 4*lsize])) / 
                  (1.0f + xkappa * shared[i + 4*lsize]);
        }
        barrier(CLK_LOCAL_MEM_FENCE);
   }

   val[ igrid ] = v;
}
