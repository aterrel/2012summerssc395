/*
 * All MDH kernels originally developed by David Gohara.
 * Extended to multiple platforms, device types, and vector
 * widths by John Stone.
 *
 * Questions about this code should be directed to
 *   David Gohara <sdg0919@gmail.com>
 *   John Stone <johns@ks.uiuc.edu>
 *
 * This kernel is in the public domain.
 *
 */

__kernel 
#if defined(WGSIZE)
__attribute__((reqd_work_group_size(WGSIZE, 1, 1)))
#endif
void mdh(  __global float *ax, __global float *ay, __global float *az,
                    __global float *charge, __global float *size, 
                    __global float *gx, __global float *gy, __global float *gz,
                    float pre1, float xkappa, __global float *val, int natoms, 
                    __local float * shared  )
{
   int igrid = get_global_id(0);
   
   float lgx = gx[igrid];
   float lgy = gy[igrid];
   float lgz = gz[igrid];
   
   float v = 0.0f;

   for(int jatom = 0; jatom < natoms; jatom++ )
   {
         float dx = lgx - ax[jatom];
         float dy = lgy - ay[jatom];
         float dz = lgz - az[jatom];

         float dist = sqrt( dx * dx + dy * dy + dz * dz );
         v += pre1 * ( charge[jatom] / dist )  * 
                exp( -xkappa * (dist - size[jatom])) / 
                (1.0f + xkappa * size[jatom]);
   }

   val[ igrid ] = v;
}


