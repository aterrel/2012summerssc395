/*
 * All MDH kernels originally developed by David Gohara.
 * Extended to multiple platforms, device types, and vector
 * widths by John Stone.
 *
 * Questions about this code should be directed to
 *   David Gohara <sdg0919@gmail.com>
 *   John Stone <johns@ks.uiuc.edu>
 *
 * This kernel is in the public domain.
 *
 */

__kernel 
#if defined(WGSIZE)
__attribute__((reqd_work_group_size(WGSIZE, 1, 1)))
#endif
void mdh(  __global float *ax, __global float *ay, __global float *az,
                    __global float *charge, __global float *size, 
                    __global FLOATVEC *gx,  
                    __global FLOATVEC *gy, 
                    __global FLOATVEC *gz,
                    float pre1, float xkappa, __global FLOATVEC *val, int natoms, 
                    __local float * shared )
{
   int igrid = get_global_id(0);
   int lsize = get_local_size(0);
   int lid = get_local_id(0);
   FLOATVEC v = 0.0f;
#if defined(ASYNCWGCOPY)
   event_t event;
#endif
 
   FLOATVEC lgx = gx[igrid];
   FLOATVEC lgy = gy[igrid];
   FLOATVEC lgz = gz[igrid];
   
   for(int jatom = 0; jatom < natoms; jatom+=lsize ) {
        if ((jatom+lsize) > natoms) 
            lsize = natoms - jatom;

#if !defined(ASYNCWGCOPY)
        barrier(CLK_LOCAL_MEM_FENCE);
        if ((jatom + lid) < natoms) {
            shared[lid          ] = ax[jatom + lid];
            shared[lid +   lsize] = ay[jatom + lid];
            shared[lid + 2*lsize] = az[jatom + lid];
            shared[lid + 3*lsize] = charge[jatom + lid];
            shared[lid + 4*lsize] = size[jatom + lid];
        }
        barrier(CLK_LOCAL_MEM_FENCE);
#else
        event = async_work_group_copy(shared          , ax+jatom, (size_t) lsize, (event_t) 0);
        event = async_work_group_copy(shared +   lsize, ay+jatom, (size_t) lsize, event);
        event = async_work_group_copy(shared + 2*lsize, az+jatom, (size_t) lsize, event);
        event = async_work_group_copy(shared + 3*lsize, charge+jatom, (size_t) lsize, event);
        event = async_work_group_copy(shared + 4*lsize, size+jatom, (size_t) lsize, event);
        wait_group_events(1, &event);
#endif
        
        for (int i=0; i<lsize; i++) {
#if defined(SHAREDMEMNOBCAST)
             // AMD/ATI GPUs can't do a broadcast to all threads from
             // a single shared memory cell, so they take a big perf hit 
             // if all threads read from the same local memory cell due to
             // resulting bank conflicts.  The workaround is to cause each
             // thread to read from a different lane, with wraparound.

             int limod = (i + lid) % lsize;
             FLOATVEC dx = lgx - shared[limod          ];
             FLOATVEC dy = lgy - shared[limod +   lsize];
             FLOATVEC dz = lgz - shared[limod + 2*lsize];
             FLOATVEC dist = sqrt(dx*dx + dy*dy + dz*dz);
             v += pre1 * (shared[limod + 3*lsize] / dist)  *
                  exp( -xkappa * (dist - shared[limod + 4*lsize])) /
                  (1.0f + xkappa * shared[limod + 4*lsize]);
#else
             FLOATVEC dx = lgx - shared[i          ];
             FLOATVEC dy = lgy - shared[i +   lsize];
             FLOATVEC dz = lgz - shared[i + 2*lsize];
             FLOATVEC dist = native_sqrt( dx * dx + dy * dy + dz * dz );
             v += pre1 * ( shared[i + 3*lsize] / dist )  *
                  native_exp( -xkappa * (dist - shared[i + 4*lsize])) /
                  (1.0f + xkappa * shared[i + 4*lsize]);
#endif
        }
   }

   val[ igrid ] = v;
}
