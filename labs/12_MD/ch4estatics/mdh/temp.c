#include<stdio.h>
#include<stdlib.h>
#include<math.h>


void cghsgo2c3_(int nx,int ny,int nz,double *x,double *rn,double *pn, double *apn,
				int itmax,int iters, double errtol, double *diagn,double *fcn,
				double *oNn,double *oEn,double *uCn){
	
	int i;
	int n = (nx-1)*ny*nz;
	int stop = n - (2*(nx-1)*ny) - 2*(nx-1);
	double rsden, rsnrm;
	
	double *x0, *x1, *x2, *x3, *x4, *x5, *x6;
	double *p0, *p1, *p2, *p3, *p4, *p5, *p6;
	double *oN, *oS, *oE, *oW, *uC, *uD;
	double *diag, *fc, *r, *p, *ap;
	
	int off1 = (nx-1)*ny+(nx-1);
	int off2 = (nx-1)*ny;
	int off3 = (nx-1)*ny+2*(nx-1);
	int off4 = (nx-1)*ny+(nx-1)-1;
	int off5 = (nx-1)*ny+(nx-1)+1;
	int off6 = (nx-1);
	int off7 = 2*(nx-1)*ny+(nx-1);
	
	x0 = &x[off1]; //x[ijk]
	x1 = &x[off2]; //x[ijm1k]
	x2 = &x[off3]; //x[ijp1k]
	x3 = &x[off4]; //x[im1jk]
	x4 = &x[off5]; //x[ip1jk]
	x5 = &x[off6]; //x[ijkm1]
	x6 = &x[off7]; //x[ijkp1]
	
	p0 = &pn[off1]; //x[ijk]
	p1 = &pn[off2]; //x[ijm1k]
	p2 = &pn[off3]; //x[ijp1k]
	p3 = &pn[off4]; //x[im1jk]
	p4 = &pn[off5]; //x[ip1jk]
	p5 = &pn[off6]; //x[ijkm1]
	p6 = &pn[off7]; //x[ijkp1]
	
	oN = &oNn[off1];
	oS = &oNn[off2];
	oE = &oEn[off1];
	oW = &oEn[off4];
	uC = &uCn[off1];
	uD = &uCn[off6];
	
	diag = &diagn[off1];
	fc = &fcn[off1];
	r = &rn[off1];
	p = &pn[off1];
	ap = &apn[off1];
	
	rsden = 0.0;
	for(i=0;i<n;i++) 
		rsden += fabs(fc[i]);
	rsnrm = rsden;
	
	for(i=0;i<stop;i++){
		r[i] = fc[i] + oN[i] * x2[i]
		+ oS[i] * x1[i]
		+ oE[i] * x4[i]
		+ oW[i] * x3[i]
		+ uC[i] * x6[i]
		+ uD[i] * x5[i]
		- diag[i] * x0[i];
	}
	
	//dumper2_(x, oN, oE, uC, diag, r, stop);
	
	double rhok1 = 0.0;
	double rhok2 = 0.0;
	for(i=0;i<stop;i++){
		rhok2 += (r[i] * r[i]);
		p[i] = r[i];
	}
	
	iters = 0;
	while(iters<itmax){
		
		double pAp = 0.0;
		for(i=0;i<stop;i++){
			ap[i] = - oN[i] * p2[i]
			- oS[i] * p1[i]
			- oE[i] * p4[i]
			- oW[i] * p3[i]
			- uC[i] * p6[i]
			- uD[i] * p5[i]
			+ diag[i] * p0[i];
			pAp += (ap[i] * p[i]);
		}
		
		double alpha = rhok2/pAp;
		rhok1 = rhok2;
		
		rsnrm = 0.0;
		rhok2 = 0.0;
		
		for(i=0;i<stop;i++){
			x0[i] += alpha * p[i];
			r[i] += -alpha * ap[i];
			rsnrm += fabs(r[i]);
			rhok2 += (r[i] * r[i]);
		}
		
		iters++;
		if((iters % 10) == 0)
			printf("Iters: %i Residual: %1.12g\n",iters,rsnrm/rsden);
		if((rsnrm/rsden) <= errtol) break;
		if(iters >= itmax) break;
		
		double beta = rhok2 / rhok1;
		double rbeta = 1. / beta;
		
		for(i=0;i<stop;i++){
			p[i] += (rbeta * r[i]);
			p[i] *= beta;
		}
	}
	
}
