/***************************************************************************
 *cr
 *cr            (C) Copyright 2007 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ***************************************************************************/
/*
 * CUDA accelerated coulombic potential grid test code
 *   John E. Stone and Chris Rodrigues
 *   http://www.ks.uiuc.edu/~johns/
 */

#include <stdio.h>
#include <stdlib.h>
#include "util.h"

#define CUERR { cudaError_t err; \
  if ((err = cudaGetLastError()) != cudaSuccess) { \
  printf("CUDA error: %s, line %d\n", cudaGetErrorString(err), __LINE__); \
  return -1; }}

// max constant buffer size is 64KB, minus whatever
// the CUDA runtime and compiler are using that we don't know about
// At 16 bytes for atom, for this program 4070 atoms is about the max
// we can store in the constant buffer.
#define MAXATOMS 4000
__constant__ float4 atominfo[MAXATOMS];

#define UNROLLX       8
#define UNROLLY       1
#define BLOCKSIZEX   16
#define BLOCKSIZEY   16
#define BLOCKSIZE    BLOCKSIZEX * BLOCKSIZEY

// This kernel calculates coulombic potential at each grid point and
// stores the results in the output array.
//
// This kernel was written by Chris Rodrigues of Wen-mei's group
//
__global__ void cenergy(int numatoms, float gridspacing, float * energygrid) {
  unsigned int xindex  = __umul24(blockIdx.x, blockDim.x) * UNROLLX
                         + threadIdx.x;
  unsigned int yindex  = __umul24(blockIdx.y, blockDim.y) + threadIdx.y;
  unsigned int outaddr = (__umul24(gridDim.x, blockDim.x) * UNROLLX) * yindex
                         + xindex;

  float coory = gridspacing * yindex;
  float coorx = gridspacing * xindex;

  float energyvalx1=0.0f;
  float energyvalx2=0.0f;
  float energyvalx3=0.0f;
  float energyvalx4=0.0f;
#if UNROLLX == 8
  float energyvalx5=0.0f;
  float energyvalx6=0.0f;
  float energyvalx7=0.0f;
  float energyvalx8=0.0f;
#endif

  float gridspacing_u = gridspacing * BLOCKSIZEX;

  //
  // XXX 59/8 FLOPS per atom
  //
  int atomid;
  for (atomid=0; atomid<numatoms; atomid++) {
    float dy = coory - atominfo[atomid].y;
    float dyz2 = (dy * dy) + atominfo[atomid].z;
    float atomq=atominfo[atomid].w;

    float dx1 = coorx - atominfo[atomid].x;
    float dx2 = dx1 + gridspacing_u;
    float dx3 = dx2 + gridspacing_u;
    float dx4 = dx3 + gridspacing_u;
#if UNROLLX == 8
    float dx5 = dx4 + gridspacing_u;
    float dx6 = dx5 + gridspacing_u;
    float dx7 = dx6 + gridspacing_u;
    float dx8 = dx7 + gridspacing_u;
#endif
    

    energyvalx1 += atomq * rsqrtf(dx1*dx1 + dyz2);
    energyvalx2 += atomq * rsqrtf(dx2*dx2 + dyz2);
    energyvalx3 += atomq * rsqrtf(dx3*dx3 + dyz2);
    energyvalx4 += atomq * rsqrtf(dx4*dx4 + dyz2);
#if UNROLLX == 8
    energyvalx5 += atomq * rsqrtf(dx5*dx5 + dyz2);
    energyvalx6 += atomq * rsqrtf(dx6*dx6 + dyz2);
    energyvalx7 += atomq * rsqrtf(dx7*dx7 + dyz2);
    energyvalx8 += atomq * rsqrtf(dx8*dx8 + dyz2);
#endif
  }

  energygrid[outaddr             ] += energyvalx1;
  energygrid[outaddr+1*BLOCKSIZEX] += energyvalx2;
  energygrid[outaddr+2*BLOCKSIZEX] += energyvalx3;
  energygrid[outaddr+3*BLOCKSIZEX] += energyvalx4;
#if UNROLLX == 8
  energygrid[outaddr+4*BLOCKSIZEX] += energyvalx5;
  energygrid[outaddr+5*BLOCKSIZEX] += energyvalx6;
  energygrid[outaddr+6*BLOCKSIZEX] += energyvalx7;
  energygrid[outaddr+7*BLOCKSIZEX] += energyvalx8;
#endif
}



int copyatomstoconstbuf(float *atoms, int count, float zplane) {
  CUERR // check and clear any existing errors

  if (count > MAXATOMS) {
    printf("Atom count exceeds constant buffer storage capacity\n");
    return -1;
  }

  float atompre[4*MAXATOMS];
  int i;
  for (i=0; i<count*4; i+=4) {
    atompre[i    ] = atoms[i    ];
    atompre[i + 1] = atoms[i + 1];
    float dz = zplane - atoms[i + 2];
    atompre[i + 2]  = dz*dz;
    atompre[i + 3] = atoms[i + 3];
  }

  cudaMemcpyToSymbol(atominfo, atompre, count * 4 * sizeof(float), 0);
  CUERR // check and clear any existing errors

  return 0;
}


int initatoms(float **atombuf, int count, dim3 volsize, float gridspacing) {
  dim3 size;
  int i;
  float *atoms;

  atoms = (float *) malloc(count * 4 * sizeof(float));
  *atombuf = atoms;

  // compute grid dimensions in angstroms
  size.x = (unsigned int) gridspacing * volsize.x;
  size.y = (unsigned int) gridspacing * volsize.y;
  size.z = (unsigned int) gridspacing * volsize.z;

  for (i=0; i<count; i++) {
    int addr = i * 4;
    atoms[addr    ] = (rand() / (float) RAND_MAX) * size.x; 
    atoms[addr + 1] = (rand() / (float) RAND_MAX) * size.y; 
    atoms[addr + 2] = (rand() / (float) RAND_MAX) * size.z; 
    atoms[addr + 3] = ((rand() / (float) RAND_MAX) * 2.0) - 1.0;  // charge
  }  

  return 0;
}


int main(int argc, char** argv) {
  float *doutput = NULL;
  float *energy = NULL;
  float *atoms = NULL;
  dim3 volsize, Gsz, Bsz;
  rt_timerhandle runtimer, mastertimer, copytimer, hostcopytimer;
  float copytotal, runtotal, mastertotal, hostcopytotal;
  const char *statestr = "|/-\\.";
  int state=0;

  printf("CUDA accelerated coulombic potential microbenchmark V4.0\n");
  printf("John E. Stone <johns@ks.uiuc.edu>\n");
  printf("and Chris Rodrigues\n");
  printf("http://www.ks.uiuc.edu/Research/gpu/\n");
  printf("--------------------------------------------------------\n");
  int deviceCount = 0;
  cudaGetDeviceCount(&deviceCount);
  printf("Detected %d CUDA accelerators:\n", deviceCount);
  int dev;
  for (dev=0; dev < deviceCount; dev++) {
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, dev);
    printf("  [%d]: '%s'  Clock: %.1f GHz  Mem: %dMB  Rev: %d.%d\n", 
           dev, deviceProp.name, 
           deviceProp.clockRate / 1000000.0f, deviceProp.totalGlobalMem / (1024*1024),
           deviceProp.major, deviceProp.minor);
  }

  int cudadev = 0;
  if (argc == 2) {
    sscanf(argv[1], "%d", &cudadev);
    if (cudadev < 0 || cudadev >= deviceCount) {
      cudadev = 0; 
    }    
  }
  printf("  Single-threaded single-GPU test run.\n");
  printf("  Opening CUDA device %d...\n", cudadev);
  cudaSetDevice(cudadev);
  CUERR // check and clear any existing errors

  // number of atoms to simulate
  int atomcount = 100000;

  // setup energy grid size
  // XXX this is a large test case to clearly illustrate that even while
  //     the CUDA kernel is running entirely on the GPU, the CUDA runtime
  //     library is soaking up the entire host CPU for some reason.
  volsize.x = 768;
  volsize.y = 768;
  volsize.z = 1;

  // set voxel spacing
  float gridspacing = 0.1;

  // setup CUDA grid and block sizes
  // XXX we have to make a trade-off between the number of threads per
  //     block and the resulting padding size we'll end up with since
  //     each thread will do several consecutive grid cells in this version,
  //     we're using up some of our available parallelism to reduce overhead.
  Bsz.x = BLOCKSIZEX;
  Bsz.y = BLOCKSIZEY;
  Bsz.z = 1;
  Gsz.x = volsize.x / (Bsz.x * UNROLLX);
  Gsz.y = volsize.y / (Bsz.y * UNROLLY); 
  Gsz.z = volsize.z; 

  // initialize the wall clock timers
  runtimer = rt_timer_create();
  mastertimer = rt_timer_create();
  copytimer = rt_timer_create();
  hostcopytimer = rt_timer_create();
  copytotal = 0;
  runtotal = 0;
  hostcopytotal = 0;

  printf("Grid size: %d x %d x %d\n", volsize.x, volsize.y, volsize.z);
  printf("Running kernel(atoms:%d, gridspacing %g, z %d)\n", atomcount, gridspacing, 0);

  // allocate and initialize atom coordinates and charges
  if (initatoms(&atoms, atomcount, volsize, gridspacing))
    return -1;

  // allocate and initialize the GPU output array
  int volmemsz = sizeof(float) * volsize.x * volsize.y * volsize.z;
  printf("Allocating %.2fMB of memory for output buffer...\n", volmemsz / (1024.0 * 1024.0));

  cudaMalloc((void**)&doutput, volmemsz);
  CUERR // check and clear any existing errors
  cudaMemset(doutput, 0, volmemsz);
  CUERR // check and clear any existing errors

printf("starting run...\n");
  rt_timer_start(mastertimer);

  int iterations=0;
  int atomstart;
  for (atomstart=0; atomstart<atomcount; atomstart+=MAXATOMS) {   
    iterations++;
    int runatoms;
    int atomsremaining = atomcount - atomstart;
    if (atomsremaining > MAXATOMS)
      runatoms = MAXATOMS;
    else
      runatoms = atomsremaining;

    printf("%c\r", statestr[state]);
    fflush(stdout);
    state = (state+1) & 3;

    // copy the atoms to the GPU
    rt_timer_start(copytimer);
    if (copyatomstoconstbuf(atoms + 4*atomstart, runatoms, 0*gridspacing)) 
      return -1;
    rt_timer_stop(copytimer);
    copytotal += rt_timer_time(copytimer);
 
    // RUN the kernel...
    rt_timer_start(runtimer);
    cenergy<<<Gsz, Bsz, 0>>>(runatoms, 0.1, doutput);
    CUERR // check and clear any existing errors
    rt_timer_stop(runtimer);
    runtotal += rt_timer_time(runtimer);
  }
  cudaThreadSynchronize();
  printf("Done\n");

  rt_timer_stop(mastertimer);
  mastertotal = rt_timer_time(mastertimer);

  // Copy the GPU output data back to the host and use/store it..
  energy = (float *) malloc(volmemsz);
  rt_timer_start(hostcopytimer);
  cudaMemcpy(energy, doutput, volmemsz,  cudaMemcpyDeviceToHost);
  CUERR // check and clear any existing errors
  rt_timer_stop(hostcopytimer);
  hostcopytotal=rt_timer_time(hostcopytimer);

#if 0
  int x, y;
  for (y=0; y<16; y++) {
    for (x=0; x<16; x++) {
      int addr = y * volsize.x + x;
      printf("out[%d]: %f\n", addr, energy[addr]);
    }
  }
#endif

  printf("Final calculation required %d iterations of %d atoms\n", iterations, MAXATOMS);
  printf("Copy time: %f seconds, %f per iteration\n", copytotal, copytotal / (float) iterations);
  printf("Kernel time: %f seconds, %f per iteration\n", runtotal, runtotal / (float) iterations);
  printf("Total time: %f seconds\n", mastertotal);
  printf("Kernel invocation rate: %f iterations per second\n", iterations / mastertotal);
  printf("GPU to host copy bandwidth: %gMB/sec, %f seconds total\n",
         (volmemsz / (1024.0 * 1024.0)) / hostcopytotal, hostcopytotal);

  double atomevalssec = ((double) volsize.x * volsize.y * volsize.z * atomcount) / (mastertotal * 1000000000.0);
  printf("Efficiency metric, %g billion atom evals per second\n", atomevalssec);

  /* 59/8 FLOPS per atom eval */
  printf("FP performance: %g GFLOPS\n", atomevalssec * (59.0/8.0));
  
  free(atoms);
  free(energy);
  cudaFree(doutput);

  return 0;
}




