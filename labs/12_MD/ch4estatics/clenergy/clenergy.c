/***************************************************************************
 *cr
 *cr            (C) Copyright 2007 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ***************************************************************************/
/*
 * OpenCL accelerated coulombic potential grid test code
 *   John E. Stone
 *   http://www.ks.uiuc.edu/~johns/
 *
 * $Id: clenergy.c,v 1.18 2010/01/11 05:14:40 johns Exp johns $
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "WKFUtils.h"
#if defined(__APPLE__)
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#define CLERR \
  if (clerr != CL_SUCCESS) {                              \
    printf("opencl error %d, line %d\n", clerr,__LINE__); \
    return -1;                                            \
  }

// max constant buffer size is 64KB, minus whatever
#define MAXATOMS 4000

#if defined(__powerpc)
// workaround for IBM Cell/PowerPC OpenCL compiler limitation
#define UNROLLX       8
#define UNROLLY       1
#define BLOCKSIZEX   16 
#define BLOCKSIZEY    1 
#define BLOCKSIZE    (BLOCKSIZEX * BLOCKSIZEY)
#else
#define UNROLLX       8
#define UNROLLY       1
#define BLOCKSIZEX    8  // make large enough to allow coalesced global mem ops
#define BLOCKSIZEY    8  // make as small as possible for finer granularity
#define BLOCKSIZE    (BLOCKSIZEX * BLOCKSIZEY)
#endif

#define V4UNROLLX       8
#define V4UNROLLY       1
#define V4BLOCKSIZEX    8 
#define V4BLOCKSIZEY    8 
#define V4BLOCKSIZE    V4BLOCKSIZEX * V4BLOCKSIZEY

// OpenCL source code
const char* clenergysrc = 
  "__kernel __attribute__((reqd_work_group_size(BLOCKSIZEX, BLOCKSIZEY, 1))) \n"
#if defined(__powerpc)
  // workaround for IBM Cell/PowerPC OpenCL compiler limitation
  "void clenergy(int numatoms, float gridspacing, __global float *energy, __global float4 *atominfo) {                        \n"
#else
  "void clenergy(int numatoms, float gridspacing, __global float *energy, __constant float4 *atominfo) {                        \n"
#endif
  "  unsigned int xindex  = (get_global_id(0) - get_local_id(0)) * UNROLLX + get_local_id(0); \n"
  "  unsigned int yindex  = get_global_id(1);                              \n"
  "  unsigned int outaddr = get_global_size(0) * UNROLLX * yindex + xindex;\n"
  "                                                                        \n"
  "  float coory = gridspacing * yindex;                                   \n"
  "  float coorx = gridspacing * xindex;                                   \n"
  "                                                                        \n"
  "  float energyvalx1 = 0.0f;                                             \n"
#if UNROLLX >= 4
  "  float energyvalx2 = 0.0f;                                             \n"
  "  float energyvalx3 = 0.0f;                                             \n"
  "  float energyvalx4 = 0.0f;                                             \n"
#endif
#if UNROLLX == 8
  "  float energyvalx5 = 0.0f;                                             \n"
  "  float energyvalx6 = 0.0f;                                             \n"
  "  float energyvalx7 = 0.0f;                                             \n"
  "  float energyvalx8 = 0.0f;                                             \n"
#endif
  "                                                                        \n"
  "  float gridspacing_u = gridspacing * BLOCKSIZEX;                       \n"
  "                                                                        \n"
  "  int atomid;                                                           \n"
  "  for (atomid=0; atomid<numatoms; atomid++) {                           \n"
  "    float dy = coory - atominfo[atomid].y;                              \n"
  "    float dyz2 = (dy * dy) + atominfo[atomid].z;                        \n"
  "                                                                        \n"
  "    float dx1 = coorx - atominfo[atomid].x;                             \n"
#if UNROLLX >= 4
  "    float dx2 = dx1 + gridspacing_u;                                    \n"
  "    float dx3 = dx2 + gridspacing_u;                                    \n"
  "    float dx4 = dx3 + gridspacing_u;                                    \n"
#endif
#if UNROLLX == 8
  "    float dx5 = dx4 + gridspacing_u;                                    \n"
  "    float dx6 = dx5 + gridspacing_u;                                    \n"
  "    float dx7 = dx6 + gridspacing_u;                                    \n"
  "    float dx8 = dx7 + gridspacing_u;                                    \n"
#endif
  "                                                                        \n"
  "    energyvalx1 += atominfo[atomid].w * native_rsqrt(dx1*dx1 + dyz2);   \n"
#if UNROLLX >= 4
  "    energyvalx2 += atominfo[atomid].w * native_rsqrt(dx2*dx2 + dyz2);   \n"
  "    energyvalx3 += atominfo[atomid].w * native_rsqrt(dx3*dx3 + dyz2);   \n"
  "    energyvalx4 += atominfo[atomid].w * native_rsqrt(dx4*dx4 + dyz2);   \n"
#endif
#if UNROLLX == 8
  "    energyvalx5 += atominfo[atomid].w * native_rsqrt(dx5*dx5 + dyz2);   \n"
  "    energyvalx6 += atominfo[atomid].w * native_rsqrt(dx6*dx6 + dyz2);   \n"
  "    energyvalx7 += atominfo[atomid].w * native_rsqrt(dx7*dx7 + dyz2);   \n"
  "    energyvalx8 += atominfo[atomid].w * native_rsqrt(dx8*dx8 + dyz2);   \n"
#endif
  "  }                                                                     \n"
  "                                                                        \n"
  "  energy[outaddr             ] += energyvalx1;                          \n"
#if UNROLLX >= 4
  "  energy[outaddr+1*BLOCKSIZEX] += energyvalx2;                          \n"
  "  energy[outaddr+2*BLOCKSIZEX] += energyvalx3;                          \n"
  "  energy[outaddr+3*BLOCKSIZEX] += energyvalx4;                          \n"
#endif
#if UNROLLX == 8
  "  energy[outaddr+4*BLOCKSIZEX] += energyvalx5;                          \n"
  "  energy[outaddr+5*BLOCKSIZEX] += energyvalx6;                          \n"
  "  energy[outaddr+6*BLOCKSIZEX] += energyvalx7;                          \n"
  "  energy[outaddr+7*BLOCKSIZEX] += energyvalx8;                          \n"
#endif
  "}                                                                       \n"
  "                                                                        \n"
  "                                                                        \n"
  "__kernel __attribute__((reqd_work_group_size(V4BLOCKSIZEX, V4BLOCKSIZEY, 1))) \n"
#if defined(__powerpc)
  // workaround for IBM Cell/PowerPC OpenCL compiler limitation
  "void clenergy_vec4(int numatoms, float gridspacing, __global float *energy, __global float4 *atominfo) {                        \n"
#else
  "void clenergy_vec4(int numatoms, float gridspacing, __global float *energy, __constant float4 *atominfo) {                        \n"
#endif
  "  unsigned int xindex  = (get_global_id(0) - get_local_id(0)) * V4UNROLLX + get_local_id(0); \n"
  "  unsigned int yindex  = get_global_id(1);                              \n"
  "  unsigned int outaddr = get_global_size(0) * V4UNROLLX * yindex + xindex;\n"
  "                                                                        \n"
  "  float coory = gridspacing * yindex;                                   \n"
  "  float coorx = gridspacing * xindex;                                   \n"
  "                                                                        \n"
  "  float4 energyvalx = 0.f;                                              \n"
#if V4UNROLLX == 8
  "  float4 energyvalx2 = 0.f;                                             \n"
#endif
  "                                                                        \n"
  "  float4 gridspacing_u4 = { 0.f, 1.f, 2.f, 3.f };                       \n"
  "  gridspacing_u4 *= gridspacing * V4BLOCKSIZEX;                         \n"
  "                                                                        \n"
  "  int atomid;                                                           \n"
  "  for (atomid=0; atomid<numatoms; atomid++) {                           \n"
  "    float dy = coory - atominfo[atomid].y;                              \n"
  "    float dyz2 = (dy * dy) + atominfo[atomid].z;                        \n"
  "                                                                        \n"
  "    float4 dx = gridspacing_u4 + (coorx - atominfo[atomid].x);          \n"
  "    energyvalx += atominfo[atomid].w * native_rsqrt(dx*dx + dyz2);      \n"
#if V4UNROLLX == 8
  "    dx += (4.0f * V4BLOCKSIZEX);                                        \n"
  "    energyvalx2 += atominfo[atomid].w * native_rsqrt(dx*dx + dyz2);     \n"
#endif
  "  }                                                                     \n"
  "                                                                        \n"
  "  energy[outaddr               ] += energyvalx.x;                       \n"
  "  energy[outaddr+1*V4BLOCKSIZEX] += energyvalx.y;                       \n"
  "  energy[outaddr+2*V4BLOCKSIZEX] += energyvalx.z;                       \n"
  "  energy[outaddr+3*V4BLOCKSIZEX] += energyvalx.w;                       \n"
#if V4UNROLLX == 8
  "  energy[outaddr+4*V4BLOCKSIZEX] += energyvalx2.x;                      \n"
  "  energy[outaddr+5*V4BLOCKSIZEX] += energyvalx2.y;                      \n"
  "  energy[outaddr+6*V4BLOCKSIZEX] += energyvalx2.z;                      \n"
  "  energy[outaddr+7*V4BLOCKSIZEX] += energyvalx2.w;                      \n"
#endif
  "}                                                                       \n"
  "                                                                        \n";


int copyatomstoconstbuf(cl_command_queue clcmdq, cl_mem datominfo,
                        float *atoms, int count, float zplane) {
  if (count > MAXATOMS) {
    printf("Atom count exceeds constant buffer storage capacity\n");
    return -1;
  }

  float atompre[4*MAXATOMS];
  int i;
  for (i=0; i<count*4; i+=4) {
    atompre[i    ] = atoms[i    ];
    atompre[i + 1] = atoms[i + 1];
    float dz = zplane - atoms[i + 2];
    atompre[i + 2]  = dz*dz;
    atompre[i + 3] = atoms[i + 3];
  }

  cl_int clerr = CL_SUCCESS;
  clerr = clEnqueueWriteBuffer(clcmdq, datominfo, CL_TRUE, 0, count * sizeof(cl_float4), (void *) atompre, 0, NULL, NULL);
  CLERR

  return 0;
}


int initatoms(float **atombuf, int count, size_t *volsize, float gridspacing) {
  float size[3];
  int i;
  float *atoms;

  atoms = (float *) malloc(count * 4 * sizeof(float));
  *atombuf = atoms;

  // compute grid dimensions in angstroms
  size[0] = gridspacing * volsize[0];
  size[1] = gridspacing * volsize[1];
  size[2] = gridspacing * volsize[2];

  for (i=0; i<count; i++) {
    int addr = i * 4;
#if 1
    atoms[addr    ] = (rand() / (float) RAND_MAX) * size[0];
    atoms[addr + 1] = (rand() / (float) RAND_MAX) * size[1];
    atoms[addr + 2] = (rand() / (float) RAND_MAX) * size[2];
    atoms[addr + 3] = ((rand() / (float) RAND_MAX) * 2.0) - 1.0;  // charge
#else
    atoms[addr    ] = 1.0;
    atoms[addr + 1] = 1.0f;
    atoms[addr + 2] = 1.0;
    atoms[addr + 3] = 0.01;
#endif
  }

  return 0;
}


int main(int argc, char** argv) {
  cl_int clerr = CL_SUCCESS;
  cl_mem doutput = NULL;
  cl_mem datominfo = NULL;
  float *energy = NULL;
  float *atoms = NULL;
  size_t volsize[3], Gsz[3], Bsz[3];
  wkf_timerhandle runtimer, mastertimer, copytimer, hostcopytimer;
  float copytotal, runtotal, mastertotal, hostcopytotal;
  const char *statestr = "|/-\\.";
  int state=0;

  int usevec4=0;
  if (getenv("VMDDCSVEC4")!=NULL)
    usevec4=1;

  printf("OpenCL accelerated coulombic potential microbenchmark V1.0\n");
  printf("John E. Stone <johns@ks.uiuc.edu>\n");
  printf("http://www.ks.uiuc.edu/Research/gpu/\n");
  printf("--------------------------------------------------------\n");

  // number of atoms to simulate
  int atomcount = 100000;

  // setup energy grid size
  volsize[0] = 768;
  volsize[1] = 768;
  volsize[2] = 1;

  // set voxel spacing
  float gridspacing = 0.1;

  // setup CUDA grid and block sizes
  // XXX we have to make a trade-off between the number of threads per
  //     block and the resulting padding size we'll end up with since
  //     each thread will do several consecutive grid cells in this version,
  //     we're using up some of our available parallelism to reduce overhead.
  if (usevec4) {
    Bsz[0] = V4BLOCKSIZEX;
    Bsz[1] = V4BLOCKSIZEY;
    Bsz[2] = 1;
    Gsz[0] = volsize[0] / V4UNROLLX;
    Gsz[1] = volsize[1] / V4UNROLLY;
    Gsz[2] = volsize[2];
  } else {
    Bsz[0] = BLOCKSIZEX;
    Bsz[1] = BLOCKSIZEY;
    Bsz[2] = 1;
    Gsz[0] = volsize[0] / UNROLLX;
    Gsz[1] = volsize[1] / UNROLLY;
    Gsz[2] = volsize[2];
  } 

printf("enumerating platforms\n"); 
  cl_platform_id platforms[16];
  cl_uint num_platforms;
  clerr = clGetPlatformIDs(16, platforms, &num_platforms);
  clerr = clGetPlatformIDs(num_platforms, platforms, NULL);

printf("getting context\n"); 
  cl_context_properties clctxprops[] = {(cl_context_properties) CL_CONTEXT_PLATFORM, (cl_context_properties) platforms[0], (cl_context_properties) 0};

  cl_context clctx = clCreateContextFromType(clctxprops, CL_DEVICE_TYPE_GPU, NULL, NULL, &clerr);
  CLERR

  size_t parmsz;
  clerr |= clGetContextInfo(clctx, CL_CONTEXT_DEVICES, 0, NULL, &parmsz);
  CLERR
printf("  found %d devices\n", parmsz / sizeof(size_t));

  cl_device_id* cldevs = (cl_device_id *) malloc(parmsz);
  clerr |= clGetContextInfo(clctx, CL_CONTEXT_DEVICES, parmsz, cldevs, NULL);
  CLERR

printf("creating command queue\n"); 
  cl_command_queue clcmdq = clCreateCommandQueue(clctx, cldevs[0], 0, &clerr);
  CLERR

printf("creating program source\n"); 
  cl_program clpgm = clCreateProgramWithSource(clctx, 1, &clenergysrc, NULL, &clerr);
  CLERR

printf("compiling program source\n"); 
  char clcompileflags[4096];
  sprintf(clcompileflags, 
          "-DUNROLLX=%d -DUNROLLY=%d -DBLOCKSIZEX=%d -DBLOCKSIZEY=%d -DBLOCKSIZE=%d " 
          "-DV4UNROLLX=%d -DV4UNROLLY=%d -DV4BLOCKSIZEX=%d -DV4BLOCKSIZEY=%d -DV4BLOCKSIZE=%d " 
          "-cl-fast-relaxed-math -cl-single-precision-constant -cl-denorms-are-zero -cl-mad-enable -cl-no-signed-zeros",
          UNROLLX, UNROLLY, BLOCKSIZEX, BLOCKSIZEY, BLOCKSIZE,
          V4UNROLLX, V4UNROLLY, V4BLOCKSIZEX, V4BLOCKSIZEY, V4BLOCKSIZE);
  clerr = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);
  if (clerr != CL_SUCCESS)
    printf("  compilation failed! (error %d)\n", clerr);

  char buildlog[8192];
  size_t len=0;
  clerr = clGetProgramBuildInfo(clpgm, cldevs[0], CL_PROGRAM_BUILD_LOG, sizeof(buildlog), buildlog, &len);
  if (len > 1) {
    printf("compilation log:\n");
    printf("  '%s'\n", buildlog);
  }
  CLERR

#if 0
  // dump PTX
  char **ptxs;
  size_t *ptxlens;
  cl_uint numdevs;
  clerr = clGetProgramInfo(clpgm, CL_PROGRAM_NUM_DEVICES, sizeof(numdevs), &numdevs, NULL);
  printf("number of devices: %d\n", numdevs);

  ptxs = (char **) malloc(numdevs * sizeof(char *));
  ptxlens = (size_t *) malloc(numdevs * sizeof(size_t));
  clerr = clGetProgramInfo(clpgm, CL_PROGRAM_BINARY_SIZES, numdevs * sizeof(size_t *), ptxlens, NULL);

  int i;
  for (i=0; i<numdevs; i++) {
    ptxs[i] = (char *) malloc(ptxlens[i]+1);
  }
  clerr = clGetProgramInfo(clpgm, CL_PROGRAM_BINARIES, numdevs * sizeof(char *), ptxs, NULL);
  if (ptxlens[0] > 1) {
    printf("Resulting PTX compilation from build:\n");
    printf("  '%s'\n", ptxs[0]);
  }
#endif


printf("creating kernel\n"); 
  cl_kernel clenergy = clCreateKernel(clpgm, "clenergy", &clerr);
  cl_kernel clenergyvec4 = clCreateKernel(clpgm, "clenergy_vec4", &clerr);
  CLERR

  // initialize the wall clock timers
  runtimer = wkf_timer_create();
  mastertimer = wkf_timer_create();
  copytimer = wkf_timer_create();
  hostcopytimer = wkf_timer_create();
  copytotal = 0;
  runtotal = 0;
  hostcopytotal = 0;

  printf("Grid size: %d x %d x %d\n", volsize[0], volsize[1], volsize[2]);
  printf("Running kernel(atoms:%d, gridspacing %g, z %d)\n", atomcount, gridspacing, 0);

printf("building and initializing atom data\n");
  // allocate and initialize atom coordinates and charges
  if (initatoms(&atoms, atomcount, volsize, gridspacing))
    return -1;

printf("allocating GPU memory\n");
  // allocate and initialize the GPU output array
  int volmemsz = sizeof(float) * volsize[0] * volsize[1] * volsize[2];
  printf("Allocating %.2fMB of memory for output buffer...\n", volmemsz / (1024.0 * 1024.0));

printf("  allocating output buffer\n");
  doutput = clCreateBuffer(clctx, CL_MEM_READ_WRITE, volmemsz, NULL, NULL);
  CLERR

printf("  allocating constant atom info buffer\n");
  datominfo = clCreateBuffer(clctx, CL_MEM_READ_ONLY, MAXATOMS * sizeof(cl_float4), NULL, NULL);
  CLERR

printf("  allocating/clearing host output buffer\n");
  energy = (float *) malloc(volmemsz);
  memset(energy, 0, volmemsz);

printf("  initializing device output buffer\n");
  clEnqueueWriteBuffer(clcmdq, doutput, CL_TRUE, 0, volmemsz, energy,
                       0, NULL, NULL);
  CLERR

printf("starting run...\n");
  wkf_timer_start(mastertimer);

  int iterations=0;
  int atomstart;
  for (atomstart=0; atomstart<atomcount; atomstart+=MAXATOMS) {   
    iterations++;
    int runatoms;
    int atomsremaining = atomcount - atomstart;
    if (atomsremaining > MAXATOMS)
      runatoms = MAXATOMS;
    else
      runatoms = atomsremaining;

    printf("%c\r", statestr[state]);
    fflush(stdout);
    state = (state+1) & 3;

    // copy the atoms to the GPU
    wkf_timer_start(copytimer);
    if (copyatomstoconstbuf(clcmdq, datominfo, 
                            atoms + 4*atomstart, runatoms, 0*gridspacing)) 
      return -1;
    wkf_timer_stop(copytimer);
    copytotal += wkf_timer_time(copytimer);
 
    // RUN the kernel...
    wkf_timer_start(runtimer);

    cl_kernel clkern;
    if (usevec4)
      clkern = clenergyvec4;
    else
      clkern = clenergy;

    clerr |= clSetKernelArg(clkern, 0, sizeof(int), &runatoms);
    clerr |= clSetKernelArg(clkern, 1, sizeof(float), &gridspacing);
    clerr |= clSetKernelArg(clkern, 2, sizeof(cl_mem), &doutput);
    clerr |= clSetKernelArg(clkern, 3, sizeof(cl_mem), &datominfo);
    CLERR
    cl_event event;
    clerr |= clEnqueueNDRangeKernel(clcmdq, clkern, 2, NULL, Gsz, Bsz, 0, NULL, &event);
    CLERR

    clerr |= clWaitForEvents(1, &event);
    CLERR

    clerr |= clReleaseEvent(event);
    CLERR

    wkf_timer_stop(runtimer);
    runtotal += wkf_timer_time(runtimer);

    if (clerr != CL_SUCCESS)
      break;

  }
  clFinish(clcmdq);
  printf("Done\n");

  wkf_timer_stop(mastertimer);
  mastertotal = wkf_timer_time(mastertimer);

  // Copy the GPU output data back to the host and use/store it..
  wkf_timer_start(hostcopytimer);
  clEnqueueReadBuffer(clcmdq, doutput, CL_TRUE, 0, volmemsz, energy,
                      0, NULL, NULL);
  wkf_timer_stop(hostcopytimer);
  hostcopytotal=wkf_timer_time(hostcopytimer);

#if 1
  int i, j;
  for (j=0; j<16; j++) {
    for (i=0; i<16; i++) {
      int addr = j*volsize[0] + i;
//      printf("[%d]%.1f ", addr, energy[addr]);
      printf("%.1f ", energy[addr]);
    }
    printf("\n");
  }
#endif

  printf("Final calculation required %d iterations of %d atoms\n", iterations, MAXATOMS);
  printf("Copy time: %f seconds, %f per iteration\n", copytotal, copytotal / (float) iterations);
  printf("Kernel time: %f seconds, %f per iteration\n", runtotal, runtotal / (float) iterations);
  printf("Total time: %f seconds\n", mastertotal);
  printf("Kernel invocation rate: %f iterations per second\n", iterations / mastertotal);
  printf("GPU to host copy bandwidth: %gMB/sec, %f seconds total\n",
         (volmemsz / (1024.0 * 1024.0)) / hostcopytotal, hostcopytotal);

  double atomevalssec = ((double) volsize[0] * volsize[1] * volsize[2] * atomcount) / (mastertotal * 1000000000.0);
  printf("Efficiency metric, %g billion atom evals per second\n", atomevalssec);

  /* 59/8 FLOPS per atom eval */
  printf("FP performance: %g GFLOPS\n", atomevalssec * (59.0/8.0));

  clReleaseKernel(clenergy);
  clReleaseKernel(clenergyvec4);
  clReleaseProgram(clpgm);
  clReleaseMemObject(doutput);
  clReleaseMemObject(datominfo);
 
  free(atoms);
  free(energy);

  return 0;
}




