/*
 * Copyright 1993-2010 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

/* Matrix multiplication: C = A * B.
 * Device code.
 */

#ifndef _MATRIXMUL_KERNEL_H_
#define _MATRIXMUL_KERNEL_H_

#include <stdio.h>

////////////////////////////////////////////////////////////////////////////////
//! Matrix multiplication on the device: C = A * B
//! wA is A's width and wB is B's width
////////////////////////////////////////////////////////////////////////////////
template <int BLOCK_SIZE> __global__ void
matrixMul( float* C, float* A, float* B, int wA, int wB)
{
  // Calculate the row index of the Pd element and M
  int Row = XXX;
  // Calculate the column idenx of Pd and N
  int Col = XXX;

  float Cvalue = 0;
  // each thread computes one element of the block sub-matrix
  for (int k = 0; k < XXX; ++k)
    Cvalue += A[XXX]*B[XXX];

  C[XXX] = Cvalue;
}

#endif // #ifndef _MATRIXMUL_KERNEL_H_
