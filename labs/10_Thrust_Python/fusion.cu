#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/reduce.h>
#include <thrust/sequence.h>

#include "book.h"

using namespace thrust;

struct square
{
  __host__ __device__
  float operator()(float x) const
  {
    return x*x;
  }
};

float snrm2_slow(const thrust::device_vector<float>& x)
{
  // without fusion
  thrust::device_vector<float> temp(x.size());
  transform(x.begin(), x.end(), temp.begin(), square());
  return sqrt( reduce(temp.begin(), temp.end()) );
}

float snrm2_fast(const thrust::device_vector<float>& x)
{
  // with fusion
  return sqrt( transform_reduce(x.begin(), x.end(), square(),
                                0.0f, plus<float>()));
}

int main(void)
{
  cudaEvent_t     start, stop;
  HANDLE_ERROR( cudaEventCreate( &start ) );
  HANDLE_ERROR( cudaEventCreate( &stop ) );
  float elapsedTime;

  thrust::device_vector<float> d_vec(1<<24);
  thrust::sequence(d_vec.begin(), d_vec.end());

  HANDLE_ERROR( cudaEventRecord( start, 0 ) );
  snrm2_slow(d_vec);
  HANDLE_ERROR( cudaEventRecord( stop, 0 ) );
  HANDLE_ERROR( cudaEventSynchronize( stop ) );
  HANDLE_ERROR( cudaEventElapsedTime( &elapsedTime,
                                      start, stop ) );
  printf("Elapsed time for snrm2_slow: %.3e ms\n", elapsedTime);

  thrust::sequence(d_vec.begin(), d_vec.end());

  HANDLE_ERROR( cudaEventRecord( start, 0 ) );
  snrm2_fast(d_vec);
  HANDLE_ERROR( cudaEventRecord( stop, 0 ) );
  HANDLE_ERROR( cudaEventSynchronize( stop ) );
  HANDLE_ERROR( cudaEventElapsedTime( &elapsedTime,
                                      start, stop ) );

  printf("Elapsed time for snrm2_fast: %.3e ms\n", elapsedTime);

}
