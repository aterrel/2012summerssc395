#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/transform.h>

struct saxpy_functor
{
  const float a;

  saxpy_functor(float a) : a( a) {}

  __host__ __device__
  float operator()(float x, float y)
  {
    return a*x + y;
  }
};

void saxpy(float a, thrust::device_vector<float>& x, thrust::device_vector<float>& y)
{
  // setup functor
  saxpy_functor func(a);
  // call transform
  transform(x.begin(), x.end(), y.begin(), y.begin(),
            func);
}

int main(void)
{
  thrust::host_vector<float> h_x(1<<24);
  thrust::host_vector<float> h_y(1<<24);
  thrust::generate(h_x.begin(), h_x.end(), rand);
  thrust::generate(h_y.begin(), h_y.end(), rand);
  thrust::device_vector<float> d_x = h_x;
  thrust::device_vector<float> d_y = h_y;
  saxpy(2.5f, d_x, d_y);
  thrust::copy(d_y.begin(), d_y.end(), h_y.begin());
}
