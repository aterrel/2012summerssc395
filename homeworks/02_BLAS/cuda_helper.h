#ifndef _CUDA_HELPER_H
#define _CUDA_HELPER_H
#include <cuda_runtime.h>
#include <sdkHelper.h>  // helper for shared functions common to CUDA SDK samples
#include <stdio.h>

///////////////////////////////////////////////////////////////////////////////
// These are CUDA Helper functions


// This will output the proper CUDA error strings in the event that a CUDA
// host call returns an error
#define checkCudaErrors(err) __checkCudaErrors (err, __FILE__, __LINE__)

inline void __checkCudaErrors( cudaError err, const char *file, const int line )
{
  if( cudaSuccess != err) {
    fprintf(stderr, "%s(%i) : CUDA Runtime API error %d: %s.\n",
	    file, line, (int)err, cudaGetErrorString( err ) );
            exit(-1);
  }
}

// This will output the proper error string when calling cudaGetLastError
#define getLastCudaError(msg)      __getLastCudaError (msg, __FILE__, __LINE__)

inline void __getLastCudaError( const char *errorMessage, const char *file, const int line )
{
  cudaError_t err = cudaGetLastError();
  if( cudaSuccess != err) {
    fprintf(stderr, "%s(%i) : getLastCudaError() CUDA error : %s : (%d) %s.\n",
	    file, line, errorMessage, (int)err, cudaGetErrorString( err ) );
    exit(-1);
  }
}


int gpuDeviceInit(int devID);
int findCudaDevice(int argc, const char **argv);
int gpuGetMaxGflopsDeviceId();

#endif // _CUDA_HELPER_H
