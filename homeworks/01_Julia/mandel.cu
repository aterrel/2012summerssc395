#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

#define ITER 1000

__device__ int compute_mandelbrot(float px, float py)
{
    float x, y, xx, yy, x_c, y_c;
    int i = 0;
    
    x_c = px;
    y_c = py;
    x=0;
    y=0;
    xx=0;
    yy=0;
    
    while ((i<ITER) && (xx+yy < 4))
    {
        y = 2.f*x*y + y_c;
        x = xx - yy + x_c;
        yy = y*y;
        xx = x*x;
        ++i;
    }
    return i;
}

__device__ int compute_julia(float px, float py)
{
    float x, y, xx, yy, x_c, y_c;
    int i = 0;
    
    x_c = -0.1724f;
    y_c = -0.652693f;
    x=px;
    y=py;
    xx=x*x;
    yy=y*y;
    
    while ((i<ITER) && (xx+yy < 4))
    {
        y = 2.f*x*y + y_c;
        x = xx - yy + x_c;
        yy = y*y;
        xx = x*x;
        ++i;
    }
    return i;
}

__global__ void make_image( int *image, int w, int h) 
{
    // thread ids
    int tx = blockIdx.x * blockDim.x + threadIdx.x;
    int ty = blockIdx.y * blockDim.y + threadIdx.y;
    int offset = tx + ty * gridDim.x * blockDim.x;

    // only compute pixels that are in the image
    if (tx < w && ty < h) {
        // pixel coordinates
        float px = -0.75f + 1.75f * (float)(w/2 - tx)/(w/2);
        float py = (float)(h/2 - ty)/(h/2);
        int iter = compute_mandelbrot(px,py);
        //int iter = compute_julia(px,py);
    
        float color = 255.f * ((ITER-iter)/(float)ITER);

	// store only one color since for greyscale rgb vals all same
        image[offset] = (int)color;
    }
}

int main( int argc, char** argv ) {

    // check if we got a width and a height
    if (argc !=3)
    {
        printf("usage: %s <width> <height>\n", argv[0]);
        exit(1);
    }

    int width, height;
    if (!(width = atoi(argv[1]))) 
    {  
        printf("ERROR: could not read width '%s'\n", argv[1]);
        exit(1);
    }
    if (!(height = atoi(argv[2])))
    {
        printf("ERROR: could not read height '%s'\n", argv[2]);
        exit(1);
    }

    int *image = new int[width*height];
    int *d_image;
    
    // allocate device image buffer, just one val since rgb are all same
    cudaMalloc( (void**)&d_image, width*height * sizeof(int) );

    dim3 threadsInBlock(16,16);
    dim3 blocksInGrid((width+15)/16, (height+15)/16);

    printf("grid: %d %d\n",blocksInGrid.x, blocksInGrid.y);

    make_image<<<blocksInGrid, threadsInBlock>>>( d_image, width, height );

    cudaMemcpy( image, d_image, width*height * sizeof(int),
		cudaMemcpyDeviceToHost );
    printf("%s\n",cudaGetErrorString(cudaGetLastError()));

    // write the image to file
    FILE* f=fopen("out.ppm","w");
    fprintf(f,"P3\n");
    fprintf(f,"# Fractal image created by Paul Navratil (pnav@tacc.utexas.edu)\n");
    fprintf(f,"%d %d\n", width, height);
    fprintf(f,"255\n");
    
    // image buffer stored bottom -> top, right -> left, so need to reverse both
    for (int i=height-1; i > -1; --i)
    {
        for (int j=width-1; j > -1; --j)
            fprintf(f,"%d %d %d  ",image[i*width+j],image[i*width+j],image[i*width+j]);
        fprintf(f,"\n");
    }
    fclose(f);

    // free the allocated memory
    cudaFree( d_image );
    delete[] image;

    return 0;
}
