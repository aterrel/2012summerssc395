#include <stdio.h>
#include <stdlib.h>

#define PRINT_MAIN 0
#define PRINT_STEPS 0

void quickSort(int arr[], int left, int right) {
      int i = left, j = right;
      int tmp;
      int pivot = arr[(left + right) / 2];

#if PRINT_STEPS
      printf("\npivot %d\n", pivot);
      printf("%d %d [ ", left, right);
      for (int i=0; i <= (right - left); ++i)
          printf("%d ", arr[i]);
      printf("]\n");
#endif  

      /* partition */
      while (i <= j) {
            while (arr[i] < pivot)
                  i++;
            while (arr[j] > pivot)
                  j--;
            if (i <= j) {
                  tmp = arr[i];
                  arr[i] = arr[j];
                  arr[j] = tmp;
                  i++;
                  j--;
            }
      };

#if PRINT_STEPS
      printf("%d %d [ ", left, right);
      for (int i=0; i <= (right - left); ++i)
          printf("%d ", arr[i]);
      printf("]\n");
#endif  
 
      /* recursion */
      if (left < j)
            quickSort(arr, left, j);
      if (i < right)
            quickSort(arr, i, right);
}

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        printf("Usage: %s <array size>\n",argv[0]);
        exit(1);
    }

    int *a;
    int size = atoi(argv[1]);
    
    a = new int[size];

    // init a
    for (int i=0; i < size; ++i)
    {
        a[i] = random() % size;
    }

#if PRINT_MAIN
    printf("in: [ ");
    for (int i=0; i<size; ++i)
	printf("%d ", a[i]);
    printf("]\n");
#endif

    quickSort(a, 0, (size-1));

#if PRINT_MAIN
    printf("\nout: [ ");
    for (int i=0; i<size; ++i)
	printf("%d ", a[i]);
    printf("]\n");
#endif

    // clean up
    delete [] a;
}

