\documentclass[12pt]{article}

\usepackage[top=1.5in, left=1in, right=1in, head=2in, bottom=1in]{geometry}

\usepackage{hyperref}
\usepackage{graphicx}
\usepackage[toc]{multitoc}
\usepackage{setspace}
\usepackage{tabularx}
\usepackage{url}

% Add TACC logo to class header
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{\includegraphics[scale=.75]{figures/TACC_logo.pdf}}
%% \chead{}
\rhead{\includegraphics[scale=.75]{figures/ut_tacc_logo.png}}
%% \lfoot{}
\cfoot{\thepage}
%% \rfoot{}
\renewcommand{\headrulewidth}{0.0pt}
\renewcommand{\footrulewidth}{0.0pt}

% Make the table of contents match
\renewcommand \thesection{\Roman{section}}
\usepackage[titles]{tocloft}
\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}
\setlength{\cftsecnumwidth}{2.4em}
\cftsetpnumwidth{.5em}
\setcounter{tocdepth}{1}

% my commands
\newcommand{\zapspace}{\topsep=0pt\partopsep=0pt\itemsep=0pt\parskip=0pt}

\begin{document}

\begin{center}
{\large High Performance Scientific Computing with GPUs}\\
{\large SSC 375/395}\\
{\large Course Unique No: 93030/93070}\\[2em]
\emph{ ``If you were plowing a field, which would you rather use: Two strong oxen or 1024 chickens?''} Seymour Cray

\emph{``If you were building an anthill, which would you rather use: 1024 ants or a bulldozer?''} Anonymous \\[2em]
Summer 2012\\
Flawn Academic Center 101B\\
10:00 am to 12:00 pm Tuesdays and Thursdays
\end{center}

\begin{tabularx}{\linewidth}{lX}
Instructors: & Paul Navr{\'a}til, Visualization Scientist,\\
             &  ROC 1.508, ACES 4.232, \url{pnav@tacc.utexas.edu} \\
             & \\
             & Andy R. Terrel, HPC Scientist,\\
             & ROC 1.436, \url{aterrel@tacc.utexas.edu}\\
             & \\
Office Hours & By appointment only \\
             & \\
%TA: None \\
%& \\
%Office Hours: \\
\end{tabularx}

\line(1,0){450}
\begin{spacing}{0.5}
%\footnotesize
\small
\tableofcontents
\end{spacing}
\line(1,0){450}

\section{Rationale}

Graphics Processing Units (GPU) are fast lightweight and highly threadable
computer processing units that have created a paradigm shift in high
performance computing (HPC). Using a GPU has become a much easier exercise with
the recent introduction of programming languages such as {\tt CUDA} and {\tt
  OpenCL}. As to the necessity of these devices, HPC codes are seeing a heavily
reliance on them from studying molecular dynamics to fluid dynamics.

\section{Course Aims and Objectives}

\subsection{Aims}

Unfortunately, not every program can take advantage of these innovative
devices.  In this class we will learn the when, where, and how to use a GPU
based code.

\subsection{Specific Learning Objectives}

\begin{itemize}
\item Write GPU Programs,
\item Understand performance trade offs of CPUs versus GPUs, and
\item Analyze application code to add GPU code.
\end{itemize}

\section{My Assumptions}

This course will include a great deal of coding and performance analysis.
Because of the advanced nature of this course we expect quite a bit of prior
knowledge, all of which is taught in the TACC SSC courses (222/292, 335/394,
and 374C/394C).  While these courses are not requirements the skills they teach
are. A partial list of these skills include:

\begin{itemize}
\item A curiosity for computational performance
\item Strong understanding of Linux programming environment
\item Working understanding of C/C++ or other OOP language
\item Significant programming experience, i.e., more than 100K lines of code
\item Familiarity with software development tools, e.g., source control
  management, text editors, compilers, debuggers, and profilers.
\end{itemize}

While any single criteria above is not a reason to drop the course, if there
are more than two you will spend a non-trivial amount of time catching up on
the subjects.


\section{Format and Procedures}

A class will consist of generally of lecture for 1 hour 15 minutes and a lab
work for 30 minutes.  Each lecture will have a lab component that re-enforces
the lecture concepts. Each week a homework assignment will be assigned on Tuesday due the next
Tuesday. There will be no final exam, but rather a final project.



\newpage

\section{Tentative Course Schedule}

\begin{tabularx}{\linewidth}{|c|X|X|X|}
\hline
{\bf Date} & {\bf Main Topic(s)} & {\bf Labs} & {\bf Evaluation} \\\hline
%
5/31 &
Class details and GPU Programming Intro &
Intro to TACC Computing Resources &
\\\hline
%
6/5 &
GPU Programming Intro and Hardware Details&
CUDA API &
Julia Assignment \\\hline
%
6/7 &
BLAS Level 1 and 2 &
sdot and saxpy &
\\\hline
%
6/12 &
BLAS Level 3 &
sgemm &
BLAS Implementation \\\hline
%
6/14 &
Sorting &
merge and bubble sort &
\\\hline
%
6/19 &
Trees &
quadtrees &
Sorting Assignment \\\hline
%
6/21 &
Intel Many Integrated Core (MIC) &
None &
\\\hline
%
6/26 &
GPU and CPU Hybrid code &
Discretre Laplaciation&
Gaussians and stencils \\\hline
%
6/28 &
GPU and CPU Streaming &
Sorting revisited &
\\\hline
%
7/3 &
Thrust and Python &
Thrust and Python examples&
Reimplement julia and sorting with libraries\\\hline
%
7/5 &
PGI Fortran and OpenACC&
Fortran examples &
\\\hline
%
7/10 &
Molecular Dynamics Codes
&
&
\\\hline
%
7/12 &
Molecular Dynamics Codes
&
&
\\\hline
%
7/17 &
Lattice Boltzman Method
&
&
\\\hline
%
7/19 &
Scheduling
&
&
\\\hline
%
7/24 &
Fast Multipole Methods
&
&
\\\hline
%
7/26 &
Finite Element Method
&
&
\\\hline
%
7/31 &
Ray Tracing &
&
\\\hline
%
8/2 &
Ray Tracing &
&
\\\hline
%
8/7 &
MultiGPU Coding &
&
\\\hline
%
8/9 &
Project Presentations&
&
\\\hline
\end{tabularx}


\subsection*{Feedback Statement}

During this course I will be asking you to give me feedback on your
learning in informal as well as formal ways, including through anonymous
surveys about how my teaching strategies are helping or hindering your
learning. It’s very important for me to know your reaction to what we’re doing
in class, so I encourage you to respond to these surveys, ensuring that
together we can create an environment effective for teaching and learning

\newpage
\section{Course Requirements}

\subsection{Class attendance and participation policy}

To make our time together as valuable as possible, we all have to work hard at
it.  The following basic principles may give us come guidelines: {\bf Every student
has the right to learn as well as the responsibility not to deprive others of
their right to learn.  Every student is accountable for his or her actions.}

In order for you to get the most out of this class, please consider the following:

\begin{itemize}
\item Attend all scheduled classes and arrive on time.  Late arrivals and early
  departures are very disruptive and violate the first basic principle listed
  above.
\item Please do not schedule other engagements during this class time.  You
  probably wouldn't appreciate it if I did!  I will try to make class as
  interesting and informative as possible, but I can't learn the material for
  you.
\item If you have trouble hearing the lecture or media presentation because of
  distractions around you, quietly ask those responsible for the distraction to
  stop.  If the distraction continues, please let me know.  It is often
  impossible for me to hear such things from my position in the classroom.
\item Please let me know immediately if you have any problem that is preventing
  you from performing satisfactorily in this class.
\end{itemize}

\subsubsection*{Religious Holy Days}

By UT Austin policy, you must notify us of your pending absence at least
fourteen days prior to the date of observance of a religious holy day. If you
must miss a class, an examination, a work assignment, or a project in order to
observe a religious holy day, We will give you an opportunity to complete the
missed work within a reasonable time after the absence.

\subsection{Course Readings/Materials}

Largely we will rely on class notes and manuscripts from the web.  Below we
list several books that may be used as reference materials but are by no means
required.

\subsubsection{Class Slides and Code}
\begin{description}
  \item {\bf Class Bitbucket site}\\
    \url{https://bitbucket.org/aterrel/2012summerssc395/overview}
\end{description}

\subsubsection{Online Sources}
\begin{description}
\item {\bf NVidia Documentation} \\
  \url{http://developer.nvidia.com/nvidia-gpu-computing-documentation}
\item {\bf CUDA C Programming Guide} \\
  \url{http://developer.download.nvidia.com/compute/DevZone/docs/html/C/doc/CUDA_C_Programming_Guide.pdf}
\item {\bf CUDA C Best Practices Guide} \\
  \url{http://developer.download.nvidia.com/compute/DevZone/docs/html/C/doc/CUDA_C_Best_Practices_Guide.pdf}
\end{description}

\subsubsection{Related Texts}
\begin{description}
\item {\bf \href{http://developer.nvidia.com/content/cuda-example-introduction-general-purpose-gpu-programming-0}{CUDA by example : an introduction to general-purpose GPU programming}}\\
  Jason Sanders, Edward Kandrot.Upper Saddle River, NJ : Addison-Wesley, c2011.\\
  (On course reserve at library.)

\item {\bf \href{http://www.elsevierdirect.com/morgan_kaufmann/kirk/}{Programming massively parallel processors : a hands-on approach}}\\
  David Kirk and Wen-mei Hwu, Morgan Kaufmann Publishers, 2010.\\
  (On course reserve at library.)

\item {\bf \href{http://mkp.com/gpu-computing-gems}{GPU Computing Gems Emerald Edition}}\\
  Wen-mei W. Hwu, Elsevier Science, 2011.\\
  (\href{http://www.utxa.eblib.com/patron/FullRecord.aspx?p=667719}{Electronic resource} at library)
\end{description}

\subsection{Assignments, Assessment, and Evaluation}

The class assessment will consist of approximately 8 weekly homework
assignments and a final project, each collectively representing half the final
grade.

\subsubsection{Homework Assigments}
\begin{description}
\item {\bf Format} Each week a set of programming tasks will be assigned.
  You will be required to turn in the implementation of those tasks along
  with a report outlining your implementation, your testing of the
  implementation, and the analysis of the implementations performance.  The
  report is expected to be about 2 pages in length.
\item {\bf Grading}
  \begin{itemize}
    \item Implementation [30\%]
      \begin{itemize}
      \item Design of the algorithms
      \item Plan for the execution
      \item Quality of code
      \end{itemize}
    \item Testing [30\%]
      \begin{itemize}
      \item Design of adequate tests
      \item Testing procedures
      \item Test results
      \end{itemize}
    \item Analysis [30\%]
      \begin{itemize}
      \item Expected performance.
      \item Reasoning for observed performance.
      \end{itemize}
    \item Overall quality of writing and presentation [10\%]
  \end{itemize}
\item {\bf Posting} Homework will be posted by the end of the day each
  Tuesday before class on Blackboard.
\item {\bf Due} Homework will be due the following Tuesday before class.
\item {\bf Late policy} Every day late will incur penalty of 10 points.  No
  homework more than three days late will be accepted.
\end{description}


\subsubsection{Final Project}

\begin{description}
\item {\bf Proposal} due 6/26

A one page description of an algorithm or application that you plan on
implementing on a GPU.  You should discuss with us what you would like to speed
up and then write out a plan for doing so.

\item {\bf Mid-Project Review} due 7/17

A two to five page report on the progress of the project.  Must include the
appropriate literature review, the basic CPU implementation details, and the
design of the GPU design.

\item {\bf Final Presentation} due 8/9

A ten minute explanation in-class of the project with an updated project review to
reflect the work.  Code for demonstration and testing must also be provided.

\end{description}


\subsubsection{ Subject-to-change notice }

Assignments and schedule is subject to change at instructor's discretion.


\subsubsection{ Critical dates }

\begin{center}
\begin{tabularx}{\linewidth}{lX}
{\bf May 31} & Classes begin for first-term, nine-week, and whole-session
  classes.\\
{\bf June 1} & Last day of the official add/drop period. After this date,
  changes in registration require the approval of the department chair and
  usually the student's dean. \\
{\bf June 5} & Last day to drop  for a possible refund. \\
{\bf June 20} & Last day a student may change to pass/fail or credit/no
  credit. \\
{\bf July 4} & Independence Day holiday. *Note our class still meets TTh* \\
{\bf August 9} & Last class day \\
{\bf August 11} & Final exam, 9:00–12:00 noon (No meeting)
\end{tabularx}
\end{center}


\subsection{Use of Blackboard in class}

 In this class we use Blackboard—a Web-based course management system
with password-protected access at http://courses.utexas.edu —to distribute
course materials, to communicate and collaborate online, to post grades, to
submit assignments, and to give you online quizzes and surveys. You can find
support in using Blackboard at the ITS Help Desk at 475-9400, Monday through
Friday, 8 a.m. to 6 p.m., so plan accordingly.

\section{Grading Procedures}

Grades will be based on:

\begin{tabular}{cc}
&\\
A: & 90\% \\
B: & 80\% \\
C: & 70\% \\
D: & 60\% \\
\end{tabular}

We reserve the right to curve the grades to the benefit of the student.

\section{Academic Integrity}

\subsection*{University of Texas Honor Code}
\begin{quote}
The core values of The University of Texas at Austin are learning, discovery,
freedom, leadership, individual opportunity, and responsibility. Each member of
the university is expected to uphold these values through integrity, honesty,
trust, fairness, and respect toward peers and community.
\end{quote}

Each student in this course is expected to abide by the University of Texas
Honor Code.  [See the UT Honor Code above.]  Any work submitted by a student in
this course for academic credit will be the student's own work.

You are encouraged to study together and to discuss information and concepts
covered in lecture and the sections with other students. You can give
"consulting" help to or receive "consulting" help from such students.  However,
this permissible cooperation should never involve one student having possession
of a copy of all or part of work done by someone else.

Should copying occur, both the student who copied work from another student and
the student who gave material to be copied will both automatically receive a
zero for the assignment. Penalty for violation of this Code can also be
extended to include failure of the course and University disciplinary action.


\section{Other University Notices and Policies}

\small

\subsection*{Use of E-mail for Official Correspondence to Students}
\begin{itemize}
\item All students should become familiar with the University's official e-mail
  student notification policy.  It is the student's responsibility to keep the
  University informed as to changes in his or her e-mail address.  Students are
  expected to check e-mail on a frequent and regular basis in order to stay
  current with University-related communications, recognizing that certain
  communications may be time-critical. It is recommended that e-mail be checked
  daily, but at a minimum, twice per week. The complete text of this policy and
  instructions for updating your e-mail address are available at
  \url{http://www.utexas.edu/its/help/utmail/1564}.
\end{itemize}

\subsection*{\bf Documented Disability Statement}
Any student with a documented disability who requires academic accommodations
should contact Services for Students with Disabilities (SSD) at (512) 471-6259
(voice) or 1-866-329-3986 (video phone).  Faculty are not required to provide
accommodations without an official accommodation letter from SSD.  (Note to
Faculty: Details of a student’s disability are confidential.  Faculty should
not ask questions related to a student’s condition or diagnosis when receiving
an official accommodation letter.)

\begin{itemize}

\item Please notify me as quickly as possible if the material being presented
  in class is not accessible (e.g., instructional videos need captioning,
  course packets are not readable for proper alternative text conversion,
  etc.).

\item Please notify me as early in the semester as possible if
  disability-related accommodations for field trips are required.  Advanced
  notice will permit the arrangement of accommodations on the given day (e.g.,
  transportation, site accessibility, etc.).

\item Contact Services for Students with Disabilities at 471-6259 (voice) or
  1-866-329-3986 (video phone) or reference SSD’s website for more
  disability-related information:
  \url{http://www.utexas.edu/diversity/ddce/ssd/for_cstudents.php}

\end{itemize}

\subsection*{\bf Behavior Concerns Advice Line (BCAL)}
If you are worried about someone who is acting differently, you may use the
Behavior Concerns Advice Line to discuss by phone your concerns about another
individual’s behavior. This service is provided through a partnership among the
Office of the Dean of Students, the Counseling and Mental Health Center (CMHC),
the Employee Assistance Program (EAP), and The University of Texas Police
Department (UTPD). Call 512-232-5050 or visit
\url{http://www.utexas.edu/safety/bcal}.

\subsection*{\bf Q drop Policy}
The State of Texas has enacted a law that limits the number of course drops for
academic reasons to six (6).  As stated in Senate Bill 1231:
\begin{quote}
  ``Beginning with the fall 2007 academic term, an institution of higher
  education may not permit an undergraduate student a total of more than six
  dropped courses, including any course a transfer student has dropped at
  another institution of higher education, unless the student shows good cause
  for dropping more than that number.''
\end{quote}

\subsection*{\bf Emergency Evacuation Policy}
Occupants of buildings on the UT Austin campus are required to evacuate and
assemble outside when a fire alarm is activated or an announcement is made.
Please be aware of the following policies regarding evacuation:
\begin{itemize}
\item Familiarize yourself with all exit doors of the classroom and the
  building. Remember that the nearest exit door may not be the one you used
  when you entered the building.

\item If you require assistance to evacuate, inform me in writing during the
  first week of class.

\item In the event of an evacuation, follow my instructions or those of class
  instructors.
\end{itemize}
Do not re-enter a building unless you’re given instructions by the Austin Fire
Department, the UT Austin Police Department, or the Fire Prevention Services
office.

\end{document}
